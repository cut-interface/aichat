"use client";

import React, { FC, useState } from "react";
import convertNumbThousand from "@/utils/convertNumbThousand";

export interface PostCardLikeActionProps {
  className?: string;
  likeCount?: number;
  liked?: boolean;
}

const PostCardLikeAction: FC<PostCardLikeActionProps> = ({
  className = "px-3 h-8 text-xs",
  likeCount = 34,
  liked = false,
}) => {
  return (
    <button
      className={`nc-PostCardLikeAction -ml-3 relative min-w-[68px] flex items-center rounded-full leading-none group transition-colors ${className} `}
      title="Resert"
    >
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-clockwise" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2z"/>
  <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466"/>
</svg>

    </button>
  );
};

export default PostCardLikeAction;
