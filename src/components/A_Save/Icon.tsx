import React, { FC } from "react";
import Save from "./Save";
import Resert from "./Resert";
import iconSave from "./Dislike";
import PostCardLikeAction from "@/components/PostCardLikeAction/PostCardLikeAction";
import PostCardCommentBtn from "@/components/PostCardCommentBtn/PostCardCommentBtn";

export interface PostCardLikeAndCommentProps {
  className?: string;
  itemClass?: string;
  hiddenCommentOnMobile?: boolean;
  useOnSinglePage?: boolean;
}

const PostCardLikeAndComment: FC<PostCardLikeAndCommentProps> = ({
  className = "",
  itemClass = "px-3 h-8 text-xs",
  hiddenCommentOnMobile = true,
  useOnSinglePage = false,
}) => {
  return (
    <div
      className={`nc-PostCardLikeAndComment flex items-center -space-x-8 rtl:space-x-reverse ${className}`}
    >
      <Save className={itemClass} />
      
    </div>
  );
};

export default PostCardLikeAndComment;
