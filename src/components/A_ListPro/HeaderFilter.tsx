"use client";
import React, { FC, useState } from "react";
import Heading from "@/components/Blog/Heading";
import Nav from "@/components/Nav/Nav";
import NavItem from "@/components/Blog/NavItem";
import Button from "../Blog/Button";
import { ArrowRightIcon } from "@heroicons/react/24/outline";

export interface HeaderFilterProps {
  tabs?: string[];
  heading: string;
}

const HeaderFilter: FC<HeaderFilterProps> = ({
  tabs = ["All items", "Garden", "Fitness", "Design"],
  heading = "🎈 Latest Articles",
}) => {
  const [tabActive, setTabActive] = useState<string>(tabs[0]);

  const handleClickTab = (item: string) => {
    if (item === tabActive) {
      return;
    }
    setTabActive(item);
  };

  return (
    <div className="relative flex flex-col mb-8">
      <Heading>{heading}</Heading>
      <Nav className="flex-wrap mt-4">
        {tabs.map((item) => (
          <NavItem
            key={item}
            onClick={() => handleClickTab(item)}
            className="mb-2 sm:mb-0 sm:mr-4"
          >
            {item}
          </NavItem>
        ))}
      </Nav>
    </div>
  );
};

export default HeaderFilter;
