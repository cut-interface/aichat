"use client";

import React, { FC, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

interface LeftNavProps {}

const LeftNav: FC<LeftNavProps> = () => {
  const [selectedItem, setSelectedItem] = useState<string | null>("/");

  const handleItemClick = (itemName: string) => {
    setSelectedItem(itemName);
  };

  return (
    <div className="z-50 flex flex-col items-center w-16 h-screen pt-5 text-black bg-white dark:bg-gray-900 border-r border-gray-300 border-solid md:w-16 lg:w-16">
      <ul className="justify-center w-full list-none">
        <Link href={"/"}>
          <li
            className={`mb-5 text-center mx-auto w-11 px-1 cursor-pointer hover:bg-gray-200 dark:hover:bg-gray-600 rounded-md  ${
              selectedItem === "/"
                ? "bg-blue-100 dark:bg-gray-600 rounded-lg"
                : ""
            }`}
            onClick={() => {
              handleItemClick("/");
            }}
          >
            <span className="mb-2 ">
              <svg
                className="w-5 h-5 mx-auto dark:text-gray-200"
                viewBox="0 0 1024 1024"
                fill="currentColor"
                height="1em"
                width="1em"
              >
                <path d="M946.5 505L560.1 118.8l-25.9-25.9a31.5 31.5 0 00-44.4 0L77.5 505a63.9 63.9 0 00-18.8 46c.4 35.2 29.7 63.3 64.9 63.3h42.5V940h691.8V614.3h43.4c17.1 0 33.2-6.7 45.3-18.8a63.6 63.6 0 0018.7-45.3c0-17-6.7-33.1-18.8-45.2zM568 868H456V664h112v204zm217.9-325.7V868H632V640c0-22.1-17.9-40-40-40H432c-22.1 0-40 17.9-40 40v228H238.1V542.3h-96l370-369.7 23.1 23.1L882 542.3h-96.1z" />
              </svg>
              <span className="block mx-auto mt-1 text-xs text-center dark:text-gray-200">
                Home
              </span>
            </span>
          </li>
        </Link>

        <Link href={"/listpro"}>
          <li
            className={`mb-5 text-center mx-auto w-11 px-1 cursor-pointer hover:bg-gray-200 dark:hover:bg-gray-800 rounded-md ${
              selectedItem === "/listpro"
                ? "bg-blue-100 dark:bg-gray-600 rounded-lg"
                : ""
            }`}
            onClick={() => handleItemClick("/listpro")}
          >
            <span className="mb-2 ">
              <svg
                className="w-5 h-5 mx-auto dark:text-gray-200"
                viewBox="0 0 1024 1024"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M464 144H160c-8.8 0-16 7.2-16 16v304c0 8.8 7.2 16 16 16h304c8.8 0 16-7.2 16-16V160c0-8.8-7.2-16-16-16zm-52 268H212V212h200v200zm452-268H560c-8.8 0-16 7.2-16 16v304c0 8.8 7.2 16 16 16h304c8.8 0 16-7.2 16-16V160c0-8.8-7.2-16-16-16zm-52 268H612V212h200v200zm52 132H560c-8.8 0-16 7.2-16 16v304c0 8.8 7.2 16 16 16h304c8.8 0 16-7.2 16-16V560c0-8.8-7.2-16-16-16zm-52 268H612V612h200v200zM424 712H296V584c0-4.4-3.6-8-8-8h-48c-4.4 0-8 3.6-8 8v128H104c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8h128v128c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V776h128c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8z" />
              </svg>
            </span>
            <span className="block mx-auto mt-1 text-xs text-center dark:text-gray-200">
              Apps
            </span>
          </li>
        </Link>

        <li
          className={`mb-5 text-center mx-auto w-11 cursor-pointer hover:bg-gray-200 dark:hover:bg-gray-800 rounded-md  ${
            selectedItem === "Flie"
              ? "bg-blue-100 dark:bg-gray-600 rounded-lg"
              : ""
          }`}
          onClick={() => handleItemClick("Flie")}
        >
          <span className="mb-2">
            <svg
              className="w-5 h-5 mx-auto dark:text-gray-200"
              viewBox="0 0 1024 1024"
              fill="currentColor"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M928 444H820V330.4c0-17.7-14.3-32-32-32H473L355.7 186.2a8.15 8.15 0 00-5.5-2.2H96c-17.7 0-32 14.3-32 32v592c0 17.7 14.3 32 32 32h698c13 0 24.8-7.9 29.7-20l134-332c1.5-3.8 2.3-7.9 2.3-12 0-17.7-14.3-32-32-32zM136 256h188.5l119.6 114.4H748V444H238c-13 0-24.8 7.9-29.7 20L136 643.2V256zm635.3 512H159l103.3-256h612.4L771.3 768z" />
            </svg>
          </span>
          <span className="block mx-auto mt-1 text-xs text-center dark:text-gray-200">
            File
          </span>
        </li>

        <Link href={"/listblog"}>
          <li
            className={`mb-5 text-center mx-auto w-11 px-1 cursor-pointer hover:bg-gray-200 dark:hover:bg-gray-800 rounded-md  ${
              selectedItem === "Stock"
                ? "bg-blue-100 dark:bg-gray-600 rounded-lg"
                : ""
            }`}
            onClick={() => handleItemClick("Learn")}
          >
            <span className="mb-2">
              <svg
                className="w-4 h-4 mx-auto dark:text-gray-200"
                viewBox="0 0 1024 1024"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M208.906 960V856h282v104c-46.667 28-94 41.333-142 40-46.667 1.333-93.333-12-140-40m276-164h-270c0-48-12-94.667-36-140s-50-83-78-113-52.667-67.333-74-112-30-91-26-139c5.333-80 36.667-148.667 94-206s142-86 254-86c113.333 0 198.333 28.667 255 86s88.333 126 95 206c2.667 40-2.667 77.667-16 113s-30.667 67.333-52 96-43 57-65 85-41 60-57 96-24 74-24 114m-378-496c-2.667 2.667-2.667 9.333 0 20s3.333 17.333 2 20c-1.333 2.667.333 9 5 19s6.667 16 6 18c-.667 2 2 8 8 18s9.667 16.333 11 19c1.333 2.667 5.667 9 13 19s12 16.333 14 19c2 2.667 7 9.667 15 21s13.333 19 16 23c58.667 81.333 96 152 112 212h82c16-62.667 53.333-133.333 112-212 2.667-4 11-15.667 25-35s22.333-31.333 25-36c2.667-4.667 8.333-14.333 17-29s14-25.667 16-33c2-7.333 4-16.667 6-28 2-11.333 2.333-23 1-35-10.667-130.667-92-196-244-196-150.667 0-231.333 65.333-242 196" />
              </svg>
            </span>
            <span className="block mx-auto mt-1 text-xs text-center dark:text-gray-200">
              Learn
            </span>
          </li>
        </Link>
        <Link href={"/listblog"}>
          <li
            className={`mb-5 text-center mx-auto w-11 px-1 cursor-pointer hover:bg-gray-200 dark:hover:bg-gray-800  rounded-md  ${
              selectedItem === "Stock"
                ? "bg-blue-100 dark:bg-gray-600 rounded-lg"
                : ""
            }`}
            onClick={() => handleItemClick("/listblog")}
          >
            <span className="">
              <svg
                className="w-5 h-5 mx-auto dark:text-gray-200"
                viewBox="0 0 24 24"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path fill="none" d="M0 0h24v24H0z" />
                <path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 100-16 8 8 0 000 16zm-5-8.5L16 8l-3.5 9.002L11 13l-4-1.5z" />
              </svg>
            </span>
            <span className="block mx-auto mt-1 text-xs text-center dark:text-gray-200">
              Learn
            </span>
          </li>
        </Link>
        <div className="w-full mt-5 mb-5 border-b-2 border-gray-300 dark:border-neutral-700" />
        {/* Thêm các mục điều hướng khác tùy thuộc vào nhu cầu của bạn */}

        <li
          className={`mb-5 text-center mx-auto w-11 px-1 cursor-pointer hover:bg-gray-200 dark:hover:bg-gray-800  rounded-md  ${
            selectedItem === "Stock"
              ? "bg-blue-100 dark:bg-gray-600 rounded-lg"
              : ""
          }`}
          onClick={() => handleItemClick("Stock")}
        >
          <span className="mb-2">
            <svg
              className="w-5 h-5 mx-auto dark:text-gray-200"
              viewBox="0 0 1024 1024"
              fill="currentColor"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M904 747H120c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8h784c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8zM165.7 621.8l39.7 39.5c3.1 3.1 8.2 3.1 11.3 0l234.7-233.9 97.6 97.3a32.11 32.11 0 0045.2 0l264.2-263.2c3.1-3.1 3.1-8.2 0-11.3l-39.7-39.6a8.03 8.03 0 00-11.3 0l-235.7 235-97.7-97.3a32.11 32.11 0 00-45.2 0L165.7 610.5a7.94 7.94 0 000 11.3z" />
            </svg>
          </span>
          <span className="block mx-auto mt-1 text-xs text-center dark:text-gray-200">
            Stock
          </span>
        </li>
      </ul>
    </div>
  );
};

export default LeftNav;
