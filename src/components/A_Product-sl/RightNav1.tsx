  "use client";
  import React, { FC, useEffect, useState } from "react";
  import { PostAuthorType } from "@/data/types";
  import Avatar from "@/components/Avatar/Avatar";
  import Link from "next/link";

  const RightNav: FC = () => {
    const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768);
    };

    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

    return (
      <>
{isMobile && (
      <div className="container relative mt-5">
      <Link
        href={"/"}
        className={`nc-CardAuthor items-center`}
      >
        <div className="flex">
        <Avatar
          sizeClass="h-10 w-10 text-base"
          containerClassName="flex-shrink-0 me-4"
          radius="rounded-full"
          userName="Nguyễn Như Hoàng"
        />
        <div>
          <h2
            className={`text-sm sm:text-base text-neutral-900 dark:text-neutral-100 font-medium sm:font-semibold`}
          >
            My Name
          </h2>
          <span
            className={`block mt-[2px] text-xs dark:text-neutral-400 text-blue-400`}
          >
            Manage account
          </span>
        </div>

        </div>

        <div className="w-full border-b border-neutral-200 dark:border-neutral-700 mt-5" />
      </Link>
      
      <div className="items-center mt-3">
          <h2
            className={`text-xs sm:text-base text-neutral-900 dark:text-neutral-100 font-medium sm:font-semibold`}
          >
            Your plan
          </h2>
          <span
            className={`block mt-[2px] text-xs dark:text-neutral-400 text-gray-400`}
          >
            Creative Cloud Free Membership
          </span>

          <div className="py-2 px-4 mt-4 bg-neutral-100 dark:bg-neutral-800 border-2 border-solid  rounded-full flex items-center justify-center leading-none text-xs font-medium hover:bg-white">
        <p>Try for free</p>
        </div>
        </div>

         <div className="w-full border-b border-neutral-200 mt-5"></div>



         <div className="items-center mt-3">
          <h2
            className={`text-xs sm:text-base text-neutral-900 dark:text-neutral-100 font-medium sm:font-semibold`}
          >
            Resources
          </h2>

          <Link href={"/"}>
          <span
            className={`block mt-[2px] text-xs dark:text-neutral-400 mb-2 text-blue-400`}
          >
            Install Creative Cloud app
          </span>
          </Link>

          <span
            className={`block mt-[2px] text-xs dark:text-neutral-400 text-blue-400 mb-2`}
          >
            Support community
          </span>
          <span
            className={`block mt-[2px] text-xs dark:text-neutral-400 text-blue-400 mb-2`}
          >
            Help center
          </span>
          <span
            className={`block mt-[2px] text-xs dark:text-neutral-400 text-blue-400 mb-2`}
          >
            Contact us
          </span>

        </div>


        </div>
    
    )}

      {!isMobile && (
      <div className=" relative -mr-28 mt-5">
      <Link
        href={"/"}
        className={`nc-CardAuthor items-center`}
      >
        <div className="flex">
        <Avatar
          sizeClass="h-10 w-10 text-base"
          containerClassName="flex-shrink-0 me-4"
          radius="rounded-full"
          userName="Nguyễn Như Hoàng"
        />
        <div>
          <h2
            className={`text-sm sm:text-base text-neutral-900 dark:text-neutral-100 font-medium sm:font-semibold`}
          >
            My Name
          </h2>
          <span
            className={`block mt-[2px] text-xs dark:text-neutral-400 text-blue-400`}
          >
            Manage account
          </span>
        </div>

        </div>

        <div className="w-full border-b border-neutral-200 dark:border-neutral-700 mt-5" />
      </Link>
      
      <div className="items-center mt-3">
          <h2
            className={`text-xs sm:text-base text-neutral-900 dark:text-neutral-100 font-medium sm:font-semibold`}
          >
            Your plan
          </h2>
          <span
            className={`block mt-[2px] text-xs dark:text-neutral-400 text-gray-400`}
          >
            Creative Cloud Free Membership
          </span>

          <div className="py-2 px-4 mt-4 bg-neutral-100 dark:bg-neutral-800 border-2 border-solid  rounded-full flex items-center justify-center leading-none text-xs font-medium hover:bg-white">
        <p>Try for free</p>
        </div>
        </div>

         <div className="w-full border-b border-neutral-200 mt-5"></div>



         <div className="items-center mt-3">
          <h2
            className={`text-xs sm:text-base text-neutral-900 dark:text-neutral-100 font-medium sm:font-semibold`}
          >
            Resources
          </h2>

          <Link href={"/"}>
          <span
            className={`block mt-[2px] text-xs dark:text-neutral-400 mb-2 text-blue-400`}
          >
            Install Creative Cloud app
          </span>
          </Link>

          <span
            className={`block mt-[2px] text-xs dark:text-neutral-400 text-blue-400 mb-2`}
          >
            Support community
          </span>
          <span
            className={`block mt-[2px] text-xs dark:text-neutral-400 text-blue-400 mb-2`}
          >
            Help center
          </span>
          <span
            className={`block mt-[2px] text-xs dark:text-neutral-400 text-blue-400 mb-2`}
          >
            Contact us
          </span>

        </div>


        </div>
    
    )}
    
      
      
      </>
    );
  };

  export default RightNav;