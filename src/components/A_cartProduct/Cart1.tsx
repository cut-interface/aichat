import React, { FC } from "react";
import Avatar from "@/components/Avatar/Avatar";
import Link from "next/link";
import Data from "@/data/jsons/__product.json";
import { PostProduct } from "@/data/types";

export interface CartProduct {
  className?: string;
  Product: PostProduct;
}

const CartProduct: FC<CartProduct> = ({Product , className}) => {
  const { title, img } = Product;
  return (
    <div>
      <div
        className={`nc-CardAuthor flex items-center h-14 space-x-3 ${className}`}
      >
        <img
          src={img}
          alt={title}
          width={"40px"}
          height={"40px"}
          className="rounded-xl"
        />

        <div>
          <span
            className={`block mt-[2px] text-sm text-gray-900 dark:text-neutral-400`}
          >
            {title}
          </span>
        </div>
      </div>
    </div>
  );
};

export default CartProduct;
