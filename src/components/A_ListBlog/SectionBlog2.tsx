import React, { FC } from "react";
import { SectionMagazine1Props } from "./SectionMagazine1";
import HeaderBlog from "./HeaderBlog";
import Cartblog from "@/components/Cartblog/Cartblog";

export interface SectionMagazine10Props extends SectionMagazine1Props {}

const Blog: FC<SectionMagazine10Props> = ({
  posts,
  heading = "Livestreams",
  className = "",
}) => {
  return (
    <div className={`nc-SectionMagazine10 ${className}`}>
        <HeaderBlog heading={heading} />
      {!posts.length && <span>Nothing we found!</span>}
      <div className="grid grid-cols-1 xl:grid-cols-1 gap-5 ">
        <div className="grid grid-cols-1 sm:grid-cols-3 sm:grid-rows-1 gap-5">
        {posts
            .filter((_, i) => i < 4 && i > 0)
            .map((item, index) => {
              return (
                <Cartblog ratio="aspect-w-5 aspect-h-3" key={index} post={item} />
              );
            })}
            
        </div>
    
     

        </div>
      </div>

  );
};

export default Blog;
