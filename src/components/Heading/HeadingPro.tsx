import React, { HTMLAttributes, ReactNode } from "react";

export interface HeadingProps extends HTMLAttributes<HTMLHeadingElement> {
  fontClass?: string;
  desc?: ReactNode;
}

const Heading1: React.FC<HeadingProps> = ({
  children,
  desc = "Discover the most outstanding articles in all topics of life. ",
  className = "mb-4 md:mb-4 text-neutral-900 dark:text-neutral-50",

  ...args
}) => {
  return (
    <div
      className={`nc-Section-Heading relative flex flex-col sm:flex-row sm:items-end justify-between ${className}`}
    >

        <h2
          className={`text-sm md:text-sm lg:text-sm font-semibold mt-4 ml-5`}
          {...args}
        >
          {children || `Section Heading`}
        </h2>
      </div>
    // </div>
  );
};

export default Heading1;
