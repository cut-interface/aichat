'use client'
import React, { FC, useState, useEffect } from "react";
import { PostAuthorType } from "@/data/types";

export interface CardAuthorProps {
  className?: string;
}

const LeftPro: FC<CardAuthorProps> = () => {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768); // Adjust the threshold as needed
    };

    handleResize(); // Initial check
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <>
      <div
        className={`fixed top-30 left-0 right-0 z-30 group border-b border-solid border-gray-300 bg-white dark:bg-neutral-900 w-full mb-10`}
        style={{
          height: "auto",
        }}
      >
        <div>
          <h2
            className={`text-sm sm:text-base font-medium sm:font-semibold p-4 text-center ${
              isMobile ? "" : ""
            }`}
          >
            Premiere Pro
          </h2>
        </div>
      </div>
    </>
  );
};

export default LeftPro;