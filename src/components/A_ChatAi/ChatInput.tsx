"use client";
import React, { FC, useState, useEffect, useCallback } from "react";
import Textarea from "../Textarea/Textarea";
import { FaArrowUp } from "react-icons/fa";
import data from "@emoji-mart/data";
import Picker from "@emoji-mart/react";
import useSWRMutation from "swr/mutation";
import MessageList from "./MessageList";
import { useMediaQuery } from "react-responsive";
interface Props {
  onSendMessage: (message: string) => void;
  messages: string[]; // Xác định kiểu dữ liệu của onSendMessage là một hàm không có tham số và không có giá trị trả về
}

const ChatInput: React.FC<Props> = ({ messages, onSendMessage }) => {
  const [loading, setLoading] = useState(false);
  const [newMessage, setNewMessage] = useState("");
  const [file, setFile] = useState(null);
  const [textContainsCharacter, setTextContainsCharacter] = useState(false);
  const [showPicker, setShowPicker] = useState(false);
  const isMobile = useMediaQuery({ maxWidth: 640 });
  const isTablet = useMediaQuery({ minWidth: 641, maxWidth: 1021 });

  const handleSendMessage = (messages: string) => {

    if (messages.trim() !== "") {
      
      onSendMessage(messages);
      setNewMessage(messages);

    }
    
  };


  


  const [content, setContent] = useState("");
  const [error, setError] = useState("");
  const [message_id, setmessage_id] = useState("");
  const [parent, setparent] = useState("");
  const [sendingMessage, setSendingMessage] = useState(false);

  // useEffect(() => {
  //   function sendRequest(url: string) {
  //     return fetch(url, {
  //       method: "GET",
  //       headers: {
  //         Accept: "application/json",
  //         "Content-Type": "application/json",
  //       },
  //     }).then((res) => res.json());
  //   }

  //   setNewMessage("");
  //   if (loading) {
  //     const pathApi = `api/message?parts=${newMessage}`;
  //     sendRequest(pathApi).then((result=200) => {
  //       setContent(result);
  //       setLoading(false);
  //     });
  //   }
  // }, [content, loading]);

  function sendRequest(url: string) {
    return fetch(url, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((res) => res.json());
  }

  const pathApi = `api/message?parts=${newMessage}`;

  const { trigger, isMutating } = useSWRMutation(pathApi, sendRequest);

 

const handleSubmit = async (
  event:
    | React.FormEvent<HTMLFormElement>
    | React.MouseEvent<HTMLButtonElement, MouseEvent>
) => {
  event.preventDefault();
  setSendingMessage(true); // Bắt đầu quá trình gửi tin nhắn
  if (newMessage.trim() !== "" && !sendingMessage) { // Kiểm tra biến sendingMessage
    handleSendMessage(newMessage);
    setShowSuggestions(false);
    setNewMessage("");


    const result = await trigger();
    if (result) {
      setContent(result);
    }
    setSendingMessage(false); // Kết thúc quá trình gửi tin nhắn
  }
};

const [suggestedQuestions, setSuggestedQuestions] = useState([
  "Bạn muốn hỏi gì123?",
  "Có điều gì bạn cần giúp đỡ?",
  "Có gì mới không?",
  "Có gì mới không?",
]);

const [showSuggestions, setShowSuggestions] = useState(true);

const handleSuggestedQuestionClick = (question: string) => {
  handleSendMessage(question);
};


  const handleTogglePicker = () => {
    setShowPicker(!showPicker);
  };



  const handleInputFocus = () => {
    if (showSuggestions) {
      setShowSuggestions(false);
    }
  };

  const handleKeyDown: React.KeyboardEventHandler<HTMLTextAreaElement> = async (
    e
  ) => {

    if (e.key === "Enter"  && !sendingMessage && !e.shiftKey) {
      e.preventDefault();

      if (newMessage.trim() !== "") {
        setSendingMessage(true); // Bắt đầu quá trình gửi tin nhắn
        handleSendMessage(newMessage);
        setNewMessage("")
        const result = await trigger();

        if(result){
          setContent(result);
        }

        setSendingMessage(false); // Kết thúc quá trình gửi tin nhắn
      }
    }
    
    if (e.target instanceof HTMLTextAreaElement) {
      setTextContainsCharacter(e.target.value.length > 0);
    }

  };

  const handleContentIdeaChange = (
    event: React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    setNewMessage(event.target.value);
  };

  return (
  
    <div className="flex flex-col items-center justify-center relative w-full ">
      <div className="w-full flex justify-center flex-1 overflow-y-auto mb-28 mt-14">
        <div
          className={`${isMobile ? "w-full" : "w-11/12"} ${
            isTablet ? "w-full" : "lg:w-7/12"
          }`}
        >
          <MessageList
            content={content}
            messages={messages}
          />
        </div>
      </div>
      <div
        className={`fixed bottom-0 w-full flex justify-center  ${
          isMobile ? "fixed bottom-0" : "container"
        }`}
      >
        <div className="w-full h-auto pt-2 z-10 dark:bg-gray-900">
          <section className="flex justify-center items-center">
            <form
              onSubmit={handleSubmit}
              className="flex justify-between -mt-4 relative mb-1 text-center w-2/3 flex-wrap"
              style={{ width: "52%", padding: "" }}
            >
              {showSuggestions && (
                <div className="">
                  {suggestedQuestions.map((question, index) => (
                    <button
                      key={index}
                      onClick={() => handleSuggestedQuestionClick(question)}
                      className="bg-gray-100 bg-opacity-60 dark:bg-gray-800 dark:text-white dark:border-gray-700 border border-solid rounded-md text-sm text-black hover:bg-gray-200 w-auto h-14 relative mb-2 mr-1 ml-1" // Thêm mb-2 để thêm khoảng cách giữa các nút
                    >
                      <div className="flex">
                        <p className="lg:w-60 m-4 text-left">{question}</p>
                        <span className="m-3">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="25"
                            height="25"
                            fill="currentColor"
                            className="bi bi-arrow-up-short"
                            viewBox="0 0 16 16"
                          >
                            <path
                              fill-rule="evenodd"
                              d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5"
                            />
                          </svg>
                        </span>
                      </div>

                      <FaArrowUp className="absolute top-0 right-0 transform translate-x-2/4 -translate-y-2/4 opacity-0 transition-opacity duration-300 ease-in-out" />
                    </button>
                  ))}
                </div>
              )}

              <textarea
                placeholder="Type your message here..."
                onFocus={handleInputFocus}
                value={newMessage}
                onChange={(e) => setNewMessage(e.target.value)}
                onKeyDown={handleKeyDown}
                className={`w-full border-2 bg-gray-200 border-gray-700 text-black dark:focus:border-white rounded-xl focus:outline-none focus:border-blue-600 flex-grow ${
                  newMessage ? "h-24" : "h-16"
                } bg-[#d9d8da]`} // Thay đổi màu nền ở đây
                style={{
                  resize: "none",
                  overflowY: "auto",
                }}
              />

              <div className="absolute bottom-16 right-20 text-neutral-500 dark:text-neutral-400 space-x-2">
                {showPicker && (
                  <Picker
                    set="apple"
                    theme="dark"
                    title="Pick your emoji…"
                    emoji="point_up"
                    showPreview={false}
                    showSkinTones={false}
                    perLine={9}
                    className="absolute top-10"
                  />
                )}
              </div>

              <div className="absolute right-6 bottom-5 flex space-x-3">
                <span className="">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="25"
                    height="25"
                    fill="currentColor"
                    className="bi bi-emoji-heart-eyes"
                    viewBox="0 0 16 16"
                    onClick={handleTogglePicker}
                  >
                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16" />
                    <path d="M11.315 10.014a.5.5 0 0 1 .548.736A4.5 4.5 0 0 1 7.965 13a4.5 4.5 0 0 1-3.898-2.25.5.5 0 0 1 .548-.736h.005l.017.005.067.015.252.055c.215.046.515.108.857.169.693.124 1.522.242 2.152.242s1.46-.118 2.152-.242a27 27 0 0 0 1.109-.224l.067-.015.017-.004.005-.002zM4.756 4.566c.763-1.424 4.02-.12.952 3.434-4.496-1.596-2.35-4.298-.952-3.434m6.488 0c1.398-.864 3.544 1.838-.952 3.434-3.067-3.554.19-4.858.952-3.434" />
                  </svg>
                </span>
                <button
                  className={` ${
                    textContainsCharacter ? "text-gray-700" : "text-gray-300"
                  }`}
                >
                  {isMutating ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-stop-circle-fill"
                      viewBox="0 0 16 16"
                    >
                      <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M6.5 5A1.5 1.5 0 0 0 5 6.5v3A1.5 1.5 0 0 0 6.5 11h3A1.5 1.5 0 0 0 11 9.5v-3A1.5 1.5 0 0 0 9.5 5z" />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="25"
                      height="25"
                      fill="currentColor"
                      className="bi bi-arrow-up-square-fill rounded-xl text-black"
                      viewBox="0 0 16 16"
                    >
                      <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0" />
                    </svg>
                  )}
                </button>
              </div>
            </form>
          </section>
          <div className="text-black text-[11px] text-center">
            <p className="pb-1">
              ChatAI có thể mắc lỗi. Hãy xem xét kiểm tra thông tin quan trọng.
            </p>
          </div>
        </div>
      </div>
    </div>
  
  );
};

export default ChatInput;
