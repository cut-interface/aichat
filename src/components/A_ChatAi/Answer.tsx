"use client";
import React from "react";
import Resert from "@/components/A_Save/Resert";
import Dislike from "@/components/A_Save/Dislike";

interface AnswerProps {
  message: string; // Định nghĩa kiểu dữ liệu cho prop message
  handleReset: () => void; // Định nghĩa kiểu dữ liệu cho prop handleReset
  handleDislikeClick: () => void; // Định nghĩa kiểu dữ liệu cho prop handleDislikeClick
}

const Answer: React.FC<AnswerProps> = ({
  message,
  handleReset,
  handleDislikeClick,
}) => {
  return (
    <div className="p-4 mb-4">
      <div className="flex space-x-1 -ml-9">
        <img
          src="https://png.pngtree.com/png-clipart/20230207/original/pngtree-beauty-logo-design-png-image_8947095.png"
          alt="img"
          width={"30px"}
          height={"30px"}
        />
        <p className="font-bold text-base pt-1">Name</p>
      </div>

      <pre className="overflow-x-auto">
        <p className="whitespace-pre-wrap">{message}</p>
      </pre>

      <div className="flex space-x-1 -ml-9 mt-10">
        <img
          src="https://png.pngtree.com/templates/sm/20180519/sm_5b001cfa899b9.jpg"
          alt="img"
          width={"30px"}
          height={"30px"}
        />
        <p className="font-extrabold text-base pt-1">Chat AI</p>
      </div>

      <div className="text-base pt-2">
        <div className="">
          <p>
            Để tạo một bảng hiện code giống như trong chatai, bạn có thể sử dụng
            HTML và CSS để tạo ra giao diện. Dưới đây là một ví dụ đơn giản về
            cách bạn có thể làm điều đó:
          </p>
          <div className="dark bg-black rounded-md mt-5 overflow-hidden">
            <div className="flex items-center bg-gray-800 relative text-token-text-secondary bg-token-surface-primary px-4 py-2 text-xs text-white font-sans justify-between rounded-t-md">
              <span>css</span>
            </div>
            <div className="">
              <code>{message}</code>
            </div>
          </div>
          <p className="mt-5">
            Đảm bảo bạn đã cài đặt React và tạo một ứng dụng React trước khi
            thực hiện các bước trên.
          </p>
          <div className="flex items-end justify-between mt-auto">
            <div className="flex items-center -space-x-10 mt-2">
              <button className="" onClick={handleReset}>
                <Resert className="ml-0 hover:text-gray-500" />
              </button>
              <button className="" onClick={handleDislikeClick}>
                <Dislike className="ml-0 hover:text-gray-500" />
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Answer;
