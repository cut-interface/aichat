"use client";
import React, { FC, useState, useEffect, useRef } from "react";
import CardAuthor from "@/components/CardAuthor/CardAuthor";
import WidgetHeading1 from "@/components/WidgetHeading1/WidgetHeading1";
import { DEMO_AUTHORS } from "@/data/authors";
import { PostAuthorType } from "@/data/types";
import { ChangeModel } from "@/data/types";
import { keyapi } from "@/data/types";
import { history } from "@/data/types";
import Link from "next/link";
import { Transition } from "@headlessui/react";
import Change from "@/data/jsons/__change_model.json";
import KeyApi from "@/data/jsons/__keyAPI.json";
import History from "@/data/jsons/__history.json";
import SwitchDarkMode2 from "@/components/SwitchDarkMode/SwitchDarkMode2";
import { CheckIcon } from "@heroicons/react/24/solid";
import ButtonPrimary from "@/components/Button/ButtonPrimary";
import ButtonSecondary from "@/components/Button/ButtonSecondary";
import Heading2 from "@/components/Heading/Heading2";
const authorsDemo: PostAuthorType[] = DEMO_AUTHORS.filter((_, i) => i < 20);

export interface WidgetAuthorsProps {
  className?: string;
  authors?: PostAuthorType[];
  changemodel?: ChangeModel[];
  keyapi?: keyapi[];
  history?: History[];
}

export const WidgetAuthors: FC<WidgetAuthorsProps> = ({
  className = "bg-gray-800 dark:bg-neutral-800 w-56",
  authors = authorsDemo,
}) => {
  const datatest = Change;
  const api = KeyApi;
  const [isMobile, setIsMobile] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const modalRef = useRef(null);
  const [showChange, setShowChange] = useState(false);
  const [showNewKeyModal, setShowNewKeyModal] = useState(false);

 const openChange = () => {
    setShowChange(true);
  };

  const closeChange = () => {
    setShowChange(false);
  };

  const handleClickOutsideChange = (event: MouseEvent) => {
    if (modalRef.current && !(modalRef.current as Node).contains(event.target as Node)) {
      closeChange();
    }
  };

  const openNewKeyModal = () => {
    setShowNewKeyModal(true);
  };

  const closeNewKeyModal = () => {
    setShowNewKeyModal(false);
  };

  const handleClickOutside = (event: MouseEvent) => {
    if (modalRef.current && !(modalRef.current as Node).contains(event.target as Node)) {
      closeNewKeyModal();
    }
  };

  const openModal = () => {
    setShowModal(true);
  };

  const closeModal = () => {
    setShowModal(false);
  };

  const handleSubmit = (event: MouseEvent) => {
    // Xử lý đăng nhập tại đây
    event.preventDefault();
    // Đóng modal sau khi đăng nhập thành công
    closeModal();
  };

  // Sử dụng useEffect để thêm event listener khi modal được mở
  useEffect(() => {
    if (showNewKeyModal) {
      // Hàm xử lý sự kiện click ra ngoài modal
      const handleClickOutside = (event: MouseEvent) => {
        if (modalRef.current && !(modalRef.current as Node).contains(event.target as Node)) {
          closeNewKeyModal();
        }
      };

      // Thêm event listener
      document.addEventListener("mousedown", handleClickOutside);

      // Cleanup: loại bỏ event listener khi modal được đóng
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }
  }, [showModal]);

  const toggleForm = () => {
    setShowForm(!showForm);
  };

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768); // Adjust the threshold as needed
    };

    handleResize(); // Initial check
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div
      className={`nc-WidgetAuthors rounded-sm fixed h-[91vh] overflow-y-auto ${
        isMobile ? "relative" : ""
      } flex flex-col flex-1 ${className}`}
    >
      <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 m-3">
        <a
          href="/chatai"
          className={`nc-CardAuthor flex items-center hover:bg-gray-400 dark:hover:bg-gray-700 bg-white h-10 w-auto rounded-xl${
            isMobile ? " mt-4" : ""
          } ${className}`}
        >
          <img
            src="https://png.pngtree.com/png-clipart/20230207/original/pngtree-beauty-logo-design-png-image_8947095.png"
            alt="img"
            width={"30px"}
            height={"30px"}
            className="m-2"
          />
          <div>
            <span
              className={`mt-[2px] text-xs text-black dark:text-white ml-2 font-bold`}
            >
              New Chat
            </span>
          </div>
        </a>
      </div>

      <div>
        <p className="text-lg font-bold m-5 text-white">Today</p>
        {History.map((test) => (
          <div>
            {test.date == "day1" && (
              <div className="flex justify-between h-10 w-auto rounded-xl m-2 relative group hover:bg-gray-500 dark:hover:bg-gray-700">
                <span className="text-xs m-3 text-gray-300 font-light dark:text-gray-300">
                  {test.title}
                </span>
                <div className="flex -space-x-3 opacity-0 group-hover:opacity-100 transition-opacity duration-200 absolute right-0 top-0">
                  <div className="relative">
                    <button
                      onClick={toggleDropdown}
                      className="inline-flex items-center justify-center w-10 h-10 text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-three-dots text-white"
                        viewBox="0 0 16 16"
                      >
                        <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3" />
                      </svg>
                    </button>
                    {isOpen && (
                      <div className="absolute right-3 top-7 mt-2 w-36 bg-white rounded-md shadow-lg z-50">
                        <a
                          href="#"
                          className="block px-4 py-2 text-gray-800 hover:bg-gray-100 hover:rounded-md"
                        >
                          <div className="flex justify-start space-x-2">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="15"
                                height="15"
                                fill="currentColor"
                                className="bi bi-share "
                                viewBox="0 0 16 16"
                              >
                                <path d="M13.5 1a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3M11 2.5a2.5 2.5 0 1 1 .603 1.628l-6.718 3.12a2.5 2.5 0 0 1 0 1.504l6.718 3.12a2.5 2.5 0 1 1-.488.876l-6.718-3.12a2.5 2.5 0 1 1 0-3.256l6.718-3.12A2.5 2.5 0 0 1 11 2.5m-8.5 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3m11 5.5a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3" />
                              </svg>
                            </span>
                            <p className="text-sm">Share</p>
                          </div>
                        </a>

                        <a
                          href="#"
                          className="block px-4 py-2 text-gray-800 hover:bg-gray-100 hover:rounded-md"
                        >
                          <div className="flex justify-start space-x-2">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="15"
                                height="15"
                                fill="currentColor"
                                className="bi bi-pencil"
                                viewBox="0 0 16 16"
                              >
                                <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325" />
                              </svg>
                            </span>
                            <p className="text-sm">Rename</p>
                          </div>
                        </a>

                        <a
                          href="#"
                          className="block px-4 py-2 text-gray-800 hover:bg-gray-100 hover:rounded-md"
                        >
                          <div className="flex justify-start space-x-2">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="15"
                                height="15"
                                fill="currentColor"
                                className="bi bi-trash3"
                                viewBox="0 0 16 16"
                              >
                                <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5" />
                              </svg>
                            </span>
                            <p className="text-sm">Delete</p>
                          </div>
                        </a>
                      </div>
                    )}
                  </div>
                  <span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="15"
                      height="15"
                      fill="currentColor"
                      className="bi bi-archive m-3 text-white"
                      viewBox="0 0 16 16"
                    >
                      <path d="M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1v7.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 12.5V5a1 1 0 0 1-1-1zm2 3v7.5A1.5 1.5 0 0 0 3.5 14h9a1.5 1.5 0 0 0 1.5-1.5V5zm13-3H1v2h14zM5 7.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5" />
                    </svg>
                  </span>
                </div>
              </div>
            )}
          </div>
        ))}
      </div>

      <div>
        <p className="text-lg font-bold text-white  m-5">Yesterday</p>
        {History.map((test) => (
          <div>
            {test.date == "yesterday" && (
              <div className="flex justify-between h-10 w-auto rounded-xl m-2 relative group hover:bg-gray-500 dark:hover:bg-gray-700">
                <span className="text-xs text-gray-300 m-3 font-light dark:text-gray-300">
                  {test.title}
                </span>
                <div className="flex -space-x-3 opacity-0 group-hover:opacity-100 transition-opacity duration-200 absolute right-0 top-0">
                  <div className="relative">
                    <button
                      onClick={toggleDropdown}
                      className="inline-flex items-center justify-center w-10 h-10 text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-three-dots text-white"
                        viewBox="0 0 16 16"
                      >
                        <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3" />
                      </svg>
                    </button>
                    {isOpen && (
                      <div className="absolute right-3 top-7 mt-2 w-36 bg-white rounded-md shadow-lg z-50">
                        <a
                          href="#"
                          className="block px-4 py-2 text-gray-800 hover:bg-gray-100 hover:rounded-md"
                        >
                          <div className="flex justify-start space-x-2">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="15"
                                height="15"
                                fill="currentColor"
                                className="bi bi-share"
                                viewBox="0 0 16 16"
                              >
                                <path d="M13.5 1a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3M11 2.5a2.5 2.5 0 1 1 .603 1.628l-6.718 3.12a2.5 2.5 0 0 1 0 1.504l6.718 3.12a2.5 2.5 0 1 1-.488.876l-6.718-3.12a2.5 2.5 0 1 1 0-3.256l6.718-3.12A2.5 2.5 0 0 1 11 2.5m-8.5 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3m11 5.5a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3" />
                              </svg>
                            </span>
                            <p className="text-sm">Share</p>
                          </div>
                        </a>

                        <a
                          href="#"
                          className="block px-4 py-2 text-gray-800 hover:bg-gray-100 hover:rounded-md"
                        >
                          <div className="flex justify-start space-x-2">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="15"
                                height="15"
                                fill="currentColor"
                                className="bi bi-pencil"
                                viewBox="0 0 16 16"
                              >
                                <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325" />
                              </svg>
                            </span>
                            <p className="text-sm">Rename</p>
                          </div>
                        </a>

                        <a
                          href="#"
                          className="block px-4 py-2 text-gray-800 hover:bg-gray-100 hover:rounded-md"
                        >
                          <div className="flex justify-start space-x-2">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="15"
                                height="15"
                                fill="currentColor"
                                className="bi bi-trash3"
                                viewBox="0 0 16 16"
                              >
                                <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5" />
                              </svg>
                            </span>
                            <p className="text-sm">Delete</p>
                          </div>
                        </a>
                      </div>
                    )}
                  </div>
                  <span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="15"
                      height="15"
                      fill="currentColor"
                      className="bi bi-archive m-3 text-white"
                      viewBox="0 0 16 16"
                    >
                      <path d="M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1v7.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 12.5V5a1 1 0 0 1-1-1zm2 3v7.5A1.5 1.5 0 0 0 3.5 14h9a1.5 1.5 0 0 0 1.5-1.5V5zm13-3H1v2h14zM5 7.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5" />
                    </svg>
                  </span>
                </div>
              </div>
            )}
          </div>
        ))}
      </div>

      <div>
        <p className="text-lg font-bold text-white m-5">Previous 7 Days</p>
        {History.map((test) => (
          <div>
            {test.date == "7day" && (
              <div className="flex justify-between h-10 w-auto rounded-xl m-2 relative group hover:bg-gray-500 dark:hover:bg-gray-700">
                <span className="text-xs  m-3 font-light dark:text-gray-300 text-gray-300">
                  {test.title}
                </span>
                <div className="flex -space-x-3 opacity-0 group-hover:opacity-100 transition-opacity duration-200 absolute right-0 top-0">
                  <div className="relative">
                    <button
                      onClick={toggleDropdown}
                      className="inline-flex items-center justify-center w-10 h-10 text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-three-dots text-white"
                        viewBox="0 0 16 16"
                      >
                        <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3" />
                      </svg>
                    </button>
                    {isOpen && (
                      <div className="absolute right-3 top-7 mt-2 w-36 bg-white rounded-md shadow-lg z-50">
                        <a
                          href="#"
                          className="block px-4 py-2 text-gray-800 hover:bg-gray-100 hover:rounded-md"
                        >
                          <div className="flex justify-start space-x-2">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="15"
                                height="15"
                                fill="currentColor"
                                className="bi bi-share"
                                viewBox="0 0 16 16"
                              >
                                <path d="M13.5 1a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3M11 2.5a2.5 2.5 0 1 1 .603 1.628l-6.718 3.12a2.5 2.5 0 0 1 0 1.504l6.718 3.12a2.5 2.5 0 1 1-.488.876l-6.718-3.12a2.5 2.5 0 1 1 0-3.256l6.718-3.12A2.5 2.5 0 0 1 11 2.5m-8.5 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3m11 5.5a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3" />
                              </svg>
                            </span>
                            <p className="text-sm">Share</p>
                          </div>
                        </a>

                        <a
                          href="#"
                          className="block px-4 py-2 text-gray-800 hover:bg-gray-100 hover:rounded-md"
                        >
                          <div className="flex justify-start space-x-2">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="15"
                                height="15"
                                fill="currentColor"
                                className="bi bi-pencil"
                                viewBox="0 0 16 16"
                              >
                                <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325" />
                              </svg>
                            </span>
                            <p className="text-sm">Rename</p>
                          </div>
                        </a>

                        <a
                          href="#"
                          className="block px-4 py-2 text-gray-800 hover:bg-gray-100 hover:rounded-md"
                        >
                          <div className="flex justify-start space-x-2">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="15"
                                height="15"
                                fill="currentColor"
                                className="bi bi-trash3"
                                viewBox="0 0 16 16"
                              >
                                <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5" />
                              </svg>
                            </span>
                            <p className="text-sm">Delete</p>
                          </div>
                        </a>
                      </div>
                    )}
                  </div>
                  <span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="15"
                      height="15"
                      fill="currentColor"
                      className="bi bi-archive m-3 text-white"
                      viewBox="0 0 16 16"
                    >
                      <path d="M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1v7.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 12.5V5a1 1 0 0 1-1-1zm2 3v7.5A1.5 1.5 0 0 0 3.5 14h9a1.5 1.5 0 0 0 1.5-1.5V5zm13-3H1v2h14zM5 7.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5" />
                    </svg>
                  </span>
                </div>
              </div>
            )}
          </div>
        ))}
      </div>

      <div>
        <p className="text-lg font-bold text-white m-5">Previous 30 Days</p>
        {History.map((test) => (
          <div>
            {test.date == "30day" && (
              <div className="flex justify-between h-10 w-auto rounded-xl m-2 relative group hover:bg-gray-500 dark:hover:bg-gray-700">
                <span className="text-xs text-gray-300 m-3 font-light dark:text-gray-300">
                  {test.title}
                </span>
                <div className="flex -space-x-3 opacity-0 group-hover:opacity-100 transition-opacity duration-200 absolute right-0 top-0">
                  <div className="relative">
                    <button
                      onClick={toggleDropdown}
                      className="inline-flex items-center justify-center w-10 h-10 text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-three-dots text-white"
                        viewBox="0 0 16 16"
                      >
                        <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3" />
                      </svg>
                    </button>
                    {isOpen && (
                      <div className="absolute right-3 top-7 mt-2 w-36 bg-white rounded-md shadow-lg z-50">
                        <a
                          href="#"
                          className="block px-4 py-2 text-gray-800 hover:bg-gray-100 hover:rounded-md"
                        >
                          <div className="flex justify-start space-x-2">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="15"
                                height="15"
                                fill="currentColor"
                                className="bi bi-share"
                                viewBox="0 0 16 16"
                              >
                                <path d="M13.5 1a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3M11 2.5a2.5 2.5 0 1 1 .603 1.628l-6.718 3.12a2.5 2.5 0 0 1 0 1.504l6.718 3.12a2.5 2.5 0 1 1-.488.876l-6.718-3.12a2.5 2.5 0 1 1 0-3.256l6.718-3.12A2.5 2.5 0 0 1 11 2.5m-8.5 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3m11 5.5a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3" />
                              </svg>
                            </span>
                            <p className="text-sm">Share</p>
                          </div>
                        </a>

                        <a
                          href="#"
                          className="block px-4 py-2 text-gray-800 hover:bg-gray-100 hover:rounded-md"
                        >
                          <div className="flex justify-start space-x-2">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="15"
                                height="15"
                                fill="currentColor"
                                className="bi bi-pencil"
                                viewBox="0 0 16 16"
                              >
                                <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325" />
                              </svg>
                            </span>
                            <p className="text-sm">Rename</p>
                          </div>
                        </a>

                        <a
                          href="#"
                          className="block px-4 py-2 text-gray-800 hover:bg-gray-100 hover:rounded-md"
                        >
                          <div className="flex justify-start space-x-2">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="15"
                                height="15"
                                fill="currentColor"
                                className="bi bi-trash3"
                                viewBox="0 0 16 16"
                              >
                                <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5" />
                              </svg>
                            </span>
                            <p className="text-sm">Delete</p>
                          </div>
                        </a>
                      </div>
                    )}
                  </div>
                  <span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="15"
                      height="15"
                      fill="currentColor"
                      className="bi bi-archive m-3 text-white"
                      viewBox="0 0 16 16"
                    >
                      <path d="M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1v7.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 12.5V5a1 1 0 0 1-1-1zm2 3v7.5A1.5 1.5 0 0 0 3.5 14h9a1.5 1.5 0 0 0 1.5-1.5V5zm13-3H1v2h14zM5 7.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5" />
                    </svg>
                  </span>
                </div>
              </div>
            )}
          </div>
        ))}
      </div>

      <div className="bottom-0 h-32 w-auto bg-gray-900  sticky dark:bg-gray-800">
        <div
          className="flex justify-start h-10 w-auto rounded-xl m-2 relative mt-5 hover:bg-gray-400 dark:hover:bg-gray-700"
          onClick={openChange}
        >
          <span className="m-3">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className="bi bi-arrow-left-right text-white"
              viewBox="0 0 16 16"
            >
              <path
                fill-rule="evenodd"
                d="M1 11.5a.5.5 0 0 0 .5.5h11.793l-3.147 3.146a.5.5 0 0 0 .708.708l4-4a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 11H1.5a.5.5 0 0 0-.5.5m14-7a.5.5 0 0 1-.5.5H2.707l3.147 3.146a.5.5 0 1 1-.708.708l-4-4a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 4H14.5a.5.5 0 0 1 .5.5"
              />
            </svg>
          </span>
          <div>
            <p className="text-xs font-bold text-white m-3">Change Model</p>
          </div>
        </div>

        <Transition
          show={showChange}
          as={React.Fragment}
          enter="transition-opacity duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="transition-opacity duration-300"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div
            className="fixed inset-0 z-50 flex items-center justify-center overflow-x-hidden overflow-y-auto outline-none focus:outline-none bg-gray-900  bg-opacity-50"
            onClick={() => handleClickOutsideChange} // Sửa lại onClick
          >
            <div
              className="relative max-w-3xl w-full md:w-11/12 mx-auto my-6 "
              ref={modalRef}
            >
              <div className="bg-white p-8 rounded-xl shadow-lg dark:bg-gray-500">
                <button
                  className="absolute top-0 right-0 m-4 mt-8 mr-7 text-gray-500 hover:text-gray-700 dark:text-gray-200"
                  onClick={closeChange}
                >
                  <svg
                    className="w-6 h-6"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M6 18L18 6M6 6l12 12"
                    />
                  </svg>
                </button>

                <h3 className="text-base font-semibold text-gray-800 mb-4 dark:text-gray-200">
                  Create new secret key
                </h3>
                {/* Your content for the new key modal here */}

                <div className="border-t border border-solid rounded-lg">
                  <div className="flex flex-col md:flex-row justify-between">
                    {Change.map((item) => (
                      <div className="w-full md:w-1/3 h-auto md:h-96 border-r border-solid p-2 space-y-3">
                        {item.id == 1 && (
                          <div className="flex">
                            <p className="p-2 font-bold text-lg dark:text-gray-200">
                              {item.name}
                            </p>
                          </div>
                        )}

                        {item.id == 2 && (
                          <div className="flex">
                            <span>
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="icon-md text-green-600 m-2"
                              >
                                <path
                                  d="M8.78158 8.60266L9.8188 5.49098C10.037 4.83634 10.963 4.83634 11.1812 5.49098L12.2184 8.60266C12.7187 10.1035 13.8965 11.2813 15.3973 11.7816L18.509 12.8188C19.1637 13.037 19.1637 13.963 18.509 14.1812L15.3973 15.2184C13.8965 15.7187 12.7187 16.8965 12.2184 18.3973L11.1812 21.509C10.963 22.1637 10.037 22.1637 9.8188 21.509L8.78158 18.3973C8.28128 16.8965 7.10354 15.7187 5.60266 15.2184L2.49098 14.1812C1.83634 13.963 1.83634 13.037 2.49098 12.8188L5.60266 11.7816C7.10354 11.2813 8.28128 10.1035 8.78158 8.60266Z"
                                  fill="currentColor"
                                ></path>
                                <path
                                  d="M17.1913 3.69537L17.6794 2.23105C17.7821 1.92298 18.2179 1.92298 18.3206 2.23105L18.8087 3.69537C19.0441 4.40167 19.5983 4.9559 20.3046 5.19133L21.769 5.67944C22.077 5.78213 22.077 6.21787 21.769 6.32056L20.3046 6.80867C19.5983 7.0441 19.0441 7.59833 18.8087 8.30463L18.3206 9.76895C18.2179 10.077 17.7821 10.077 17.6794 9.76895L17.1913 8.30463C16.9559 7.59833 16.4017 7.0441 15.6954 6.80867L14.231 6.32056C13.923 6.21787 13.923 5.78213 14.231 5.67944L15.6954 5.19133C16.4017 4.9559 16.9559 4.40167 17.1913 3.69537Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <p className="p-2 font-bold text-lg dark:text-gray-200">
                              {item.name}
                            </p>
                          </div>
                        )}

                        {item.id == 3 && (
                          <div className="flex">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 16 16"
                                fill="none"
                                className="icon-md text-blue-800 m-2"
                                width="20"
                                height="20"
                              >
                                <path
                                  d="M12.784 1.442a.8.8 0 0 0-1.569 0l-.191.953a.8.8 0 0 1-.628.628l-.953.19a.8.8 0 0 0 0 1.57l.953.19a.8.8 0 0 1 .628.629l.19.953a.8.8 0 0 0 1.57 0l.19-.953a.8.8 0 0 1 .629-.628l.953-.19a.8.8 0 0 0 0-1.57l-.953-.19a.8.8 0 0 1-.628-.629l-.19-.953h-.002ZM5.559 4.546a.8.8 0 0 0-1.519 0l-.546 1.64a.8.8 0 0 1-.507.507l-1.64.546a.8.8 0 0 0 0 1.519l1.64.547a.8.8 0 0 1 .507.505l.546 1.641a.8.8 0 0 0 1.519 0l.546-1.64a.8.8 0 0 1 .506-.507l1.641-.546a.8.8 0 0 0 0-1.519l-1.64-.546a.8.8 0 0 1-.507-.506L5.56 4.546Zm5.6 6.4a.8.8 0 0 0-1.519 0l-.147.44a.8.8 0 0 1-.505.507l-.441.146a.8.8 0 0 0 0 1.519l.44.146a.8.8 0 0 1 .507.506l.146.441a.8.8 0 0 0 1.519 0l.147-.44a.8.8 0 0 1 .506-.507l.44-.146a.8.8 0 0 0 0-1.519l-.44-.147a.8.8 0 0 1-.507-.505l-.146-.441Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <p className="p-2 font-bold text-lg dark:text-gray-200">
                              {item.name}
                            </p>
                          </div>
                        )}
                        <span className="text-sm p-2 dark:text-gray-200">
                          {item.price}
                        </span>

                        <div>
                          {item.upgrade === "Your current plan" && (
                            <button className="w-full h-12 bg-gray-300 rounded-xl sticky bottom-0">
                              <p className="text-sm font-semibold text-gray-600 ">
                                {item.upgrade}
                              </p>
                            </button>
                          )}
                        </div>

                        <div>
                          {item.upgrade === "Upgrade to Plus" && (
                            <button className="w-full h-12 bg-green-600 rounded-xl sticky bottom-0">
                              <p className="text-sm font-semibold text-white">
                                {item.upgrade}
                              </p>
                            </button>
                          )}
                        </div>

                        <div>
                          {item.upgrade === "Upgrade to Team" && (
                            <button className="w-full h-12 bg-blue-600 rounded-xl sticky bottom-0">
                              <p className="text-sm font-semibold text-white">
                                {item.upgrade}
                              </p>
                            </button>
                          )}
                        </div>
                        <div className="text-xs">
                          <div className="m-2">
                            <h2 className="font-bold">{item.title}</h2>
                          </div>
                          <div className="m-2 flex space-x-3">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="16"
                                height="16"
                                fill="currentColor"
                                className="bi bi-check2"
                                viewBox="0 0 16 16"
                              >
                                <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0" />
                              </svg>
                            </span>

                            <span>{item.function1}</span>
                          </div>
                          <div className="m-2 flex space-x-3">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="16"
                                height="16"
                                fill="currentColor"
                                className="bi bi-check2"
                                viewBox="0 0 16 16"
                              >
                                <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0" />
                              </svg>
                            </span>
                            <span>{item.function2}</span>
                          </div>
                          <div className="m-2 flex space-x-3">
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="16"
                                height="16"
                                fill="currentColor"
                                className="bi bi-check2"
                                viewBox="0 0 16 16"
                              >
                                <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0" />
                              </svg>
                            </span>
                            <span>{item.function3}</span>
                          </div>
                          {item.function4 && ( // Sử dụng && để kiểm tra nếu item.function4 tồn tại
                            <div className="m-2 flex space-x-3">
                              <span>
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="16"
                                  height="16"
                                  fill="currentColor"
                                  className="bi bi-check2"
                                  viewBox="0 0 16 16"
                                >
                                  <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0" />
                                </svg>
                              </span>
                              <span>{item.function4}</span>
                            </div>
                          )}
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Transition>

        <Transition
          show={showModal}
          as={React.Fragment}
          enter="transition-opacity duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="transition-opacity duration-300"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 z-50 flex items-center justify-center overflow-x-hidden overflow-y-auto outline-none focus:outline-none bg-gray-900 bg-opacity-50">
            <div className="relative max-w-3xl w-full md:w-11/12 mx-auto my-6">
              <div className="bg-white p-8 rounded-xl shadow-lg dark:bg-gray-700">
                <button
                  className="absolute top-0 right-0 m-4 mt-8 mr-7 text-gray-500 hover:text-gray-700 dark:text-gray-200"
                  onClick={closeModal}
                >
                  <svg
                    className="w-6 h-6"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M6 18L18 6M6 6l12 12"
                    />
                  </svg>
                </button>
                <h3 className="text-xl font-semibold text-gray-800 mb-4 dark:text-gray-200">
                  API keys
                </h3>
                <div className="border-b-2 w-full"></div>

                <div className="mt-5 space-y-5">
                  <p className="text-sm">
                    Your secret API keys are listed below. Please note that we
                    do not display your secret API keys again after you generate
                    them.
                  </p>
                  <p className="text-sm">
                    Do not share your API key with others, or expose it in the
                    browser or other client-side code. In order to protect the
                    security of your account, OpenAI may also automatically
                    disable any API key that we've found has leaked publicly.
                  </p>
                  <p className="text-sm">
                    Enable tracking to see usage per API key on the{" "}
                    <a href="" className="text-blue-600">
                      Usage page
                    </a>
                    .
                  </p>
                </div>

                <table className="mt-3">
                  <thead className="">
                    <tr
                      className="font-semibold text-10px"
                      style={{ fontSize: "11px" }}
                    >
                      <th>NAME</th>
                      <th className="md:pl-32">SECRET KEY</th>
                      <th className="md:pl-4">TRACKING</th>
                      <th className="md:pl-4">CREATED</th>
                      <th className="md:pl-4">LAST USED</th>
                      <th className="md:pl-4">PERMISSIONS</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody className="font-light text-xs">
                    {KeyApi.map((item) => (
                      <tr>
                        <td>{item.name}</td>
                        <td className="md:pl-32">{item.secret}</td>
                        <td className="md:pl-4">{item.tracking}</td>
                        <td className="md:pl-4">{item.created}</td>
                        <td className="md:pl-4">{item.used}</td>
                        <td className="md:pl-4">{item.premi}</td>
                        <td className="md:pl-4">
                          <span>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              fill="currentColor"
                              className="bi bi-trash3"
                              viewBox="0 0 16 16"
                            >
                              <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5" />
                            </svg>
                          </span>
                        </td>
                        <td className="md:pl-2">
                          <span>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              fill="currentColor"
                              className="bi bi-pencil-square"
                              viewBox="0 0 16 16"
                            >
                              <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                              <path
                                fillRule="evenodd"
                                d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"
                              />
                            </svg>
                          </span>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>

                <div className="">
                  <div
                    className="w-full md:w-44 h-9 bg-gray-300 border-2 rounded-xl flex justify-start items-center mt-5 space-x-2 hover:bg-gray-200 dark:bg-gray-600"
                    onClick={openNewKeyModal}
                  >
                    <span>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="20"
                        height="20"
                        fill="currentColor"
                        className="bi bi-plus ml-2 font-bold"
                        viewBox="0 0 16 16"
                      >
                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4" />
                      </svg>
                    </span>
                    <span className="text-center text-xs font-bold ">
                      <p>Create new key</p>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Transition>

        <Transition
          show={showNewKeyModal}
          as={React.Fragment}
          enter="transition-opacity duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="transition-opacity duration-300"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div
            className="fixed inset-0 z-50 flex items-center justify-center overflow-x-hidden overflow-y-auto outline-none focus:outline-none bg-gray-900 bg-opacity-50 "
            onClick={() => handleClickOutside}
          >
            <div
              className="relative max-w-xl w-4/12 mx-auto my-6"
              ref={modalRef}
            >
              <div className="bg-white p-8 rounded-xl shadow-lg dark:bg-gray-900">
                <h3 className="text-base font-semibold text-gray-800 mb-4 dark:text-gray-200">
                  Create new secret key
                </h3>
                {/* Your content for the new key modal here */}

                <div className="border-b-2 dark:border-gray-500"></div>
                <div>
                  <p className="text-sm mt-3 mb-3 font-semibold">Name</p>
                </div>
                <input
                  type="text"
                  placeholder="My test key"
                  className="w-full rounded-lg h-8 dark:bg-gray-800"
                />

                <div className="flex justify-end space-x-4 mt-5">
                  <button
                    className="w-auto h-9 bg-gray-400 rounded-lg "
                    onClick={closeNewKeyModal}
                  >
                    <p className="text-xs m-2 font-bold text-white">Cancel</p>
                  </button>

                  <button className="w-auto h-9 bg-green-700 rounded-lg">
                    <p className="text-xs m-2 font-bold text-white">
                      Create secret key
                    </p>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Transition>

        {showForm && (
          <div className="ml-2 absolute bg-white w-48 -mt-28 rounded-lg dark:bg-gray-700">
            <div className="">
              <div>
                <div className="flex justify-start h-10 w-auto rounded-xl m-2 relative mt-2 hover:bg-gray-300 dark:hover:bg-gray-900">
                  <span className="m-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-arrow-left-right"
                      viewBox="0 0 16 16"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M1 11.5a.5.5 0 0 0 .5.5h11.793l-3.147 3.146a.5.5 0 0 0 .708.708l4-4a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 11H1.5a.5.5 0 0 0-.5.5m14-7a.5.5 0 0 1-.5.5H2.707l3.147 3.146a.5.5 0 1 1-.708.708l-4-4a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 4H14.5a.5.5 0 0 1 .5.5"
                      />
                    </svg>
                  </span>
                  <p className="m-2 font-semibold text-black text-[11px] dark:text-gray-200">
                    Dark mode
                  </p>
                  <div className="m-2">
                    <SwitchDarkMode2 />
                  </div>
                </div>
              </div>

              <div
                className="flex justify-start h-10 w-auto rounded-xl m-2 relative mt-5 hover:bg-gray-300 dark:hover:bg-gray-900"
                onClick={openModal}
              >
                <span className="m-3">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    className="bi bi-key"
                    viewBox="0 0 16 16"
                  >
                    <path d="M0 8a4 4 0 0 1 7.465-2H14a.5.5 0 0 1 .354.146l1.5 1.5a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0L13 9.207l-.646.647a.5.5 0 0 1-.708 0L11 9.207l-.646.647a.5.5 0 0 1-.708 0L9 9.207l-.646.647A.5.5 0 0 1 8 10h-.535A4 4 0 0 1 0 8m4-3a3 3 0 1 0 2.712 4.285A.5.5 0 0 1 7.163 9h.63l.853-.854a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.793-.793-1-1h-6.63a.5.5 0 0 1-.451-.285A3 3 0 0 0 4 5" />
                    <path d="M4 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0" />
                  </svg>
                </span>
                <p className="m-2 font-semibold text-black text-[11px] dark:text-gray-200">
                  API KEY
                </p>
              </div>
              {/* Your form content here */}
            </div>
          </div>
        )}

        <div
          className="flex justify-start h-10 w-auto rounded-xl mt-5 m-2 relative group hover:bg-gray-400 dark:hover:bg-gray-700"
          onClick={toggleForm}
        >
          <span className="m-3">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="18"
              height="18"
              fill="currentColor"
              className="bi bi-gear text-white"
              viewBox="0 0 16 16"
            >
              <path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492M5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0" />
              <path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115z" />
            </svg>
          </span>
          <p className="m-3 font-bold  text-xs text-white">Setting</p>
        </div>
      </div>
    </div>
  );
};

export default WidgetAuthors;
