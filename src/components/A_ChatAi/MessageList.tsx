"use client";
import React, { Fragment, useEffect, useState, useRef } from "react";
import Save from "@/components/A_Save/Save";
import Resert from "@/components/A_Save/Resert";
import Dislike from "@/components/A_Save/Dislike";
import { CodeBlock, dracula } from "react-code-blocks";
import { WidgetAuthors } from "@/components/A_ChatAi/MenuRight";
import Icon from "@/components/Support/Icon";
import {
  ShoppingBagIcon as ShoppingCartIcon,
  Cog8ToothIcon as CogIcon,
} from "@heroicons/react/24/outline";
import { Popover, Transition } from "@headlessui/react";
import SwitchDarkMode2 from "@/components/SwitchDarkMode/SwitchDarkMode2";
import { feedback } from "@/data/types";
import datafeedback from "@/data/jsons/__feedback.json";
import { listMessage } from "@/data/types";
import dataListMessage from "@/data/jsons/__listMessage.json";
import { stringify } from "querystring";
import useSWR from "swr";

const List = dataListMessage;
const Feed = datafeedback;
interface Props {
  content: {
    children: string;
    conversation_id: string;
    message_id: string;
    parent: string;
    parts: string;
    role: string;
    error: string;
  };

  messages: string[];
  feedback?: feedback[];
  listMessage?: listMessage[];
}

const MessageList: React.FC<Props> = ({
  content,
  messages,
  feedback = Feed,
  listMessage = List,
}) => {
  const messagesEndRef = useRef<HTMLDivElement>(null);
  const divRef = useRef<HTMLDivElement>(null);
  const [copied, setCopied] = useState(false);
  const [showQuestions, setShowQuestions] = useState(false);
  const [thankYou, setThankYou] = useState(false);
  const [text, setText] = useState("");
  const [isChanging, setIsChanging] = useState(false);
  const initialShowAnswers = Array.isArray(content)
    ? content.map(() => false)
    : [];
  const [showAnswers, setShowAnswers] = useState(initialShowAnswers);
  const [showModal, setModal] = useState(false);
  const handleReset = (index: number) => {
    const updatedAnswers = [...showAnswers];
    updatedAnswers[index] = false;
    setShowAnswers(updatedAnswers);
  };
  const [render, setRender] = useState({});
  const [loading, setLoading] = useState(true);

  const openModal = () => {
    setModal(true);
  };

  const cloneModal = () => {
    setModal(false);
  };

  const handleCloseModal = () => {
    cloneModal();
    handleQuestionClick();
  };

  const handleDislikeClick = (index: string) => {
    const updatedAnswers = [...showAnswers];
    updatedAnswers[index] = true;
    setShowAnswers(updatedAnswers);
    setShowQuestions(true);
  };

  const handleQuestionClick = () => {
    setThankYou(true);
    setTimeout(() => {
      setThankYou(false);
      setShowQuestions(false);
    }, 3000);
  };

  useEffect(() => {
    if (content) {
      // Kiểm tra content có tồn tại và không đang trong quá trình load dữ liệu mới
      setLoading(false); // Đặt loading thành false khi content đã được set và không đang load dữ liệu mới
    }

    if (messagesEndRef.current) {
      messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
    }
  }, [content, messages]);

  const questions = [
    "Don't like the style",
    "Not factually correct",
    "Didn't fully follow instructions",
    "Refused when it shouldn't have",
    "Being lazy",
    "More...",
  ];

  const handleChangeMessage = () => {
    setIsChanging(true);
    setTimeout(() => {
      setText("Goodbye");
      setIsChanging(false);
    }, 3000);
  };

  const handleCopyCode = () => {
    if (divRef.current) {
      const divContent = divRef.current.innerText;
      navigator.clipboard
        .writeText(divContent)
        .then(() => {
          console.log("Content copied successfully");
        })
        .catch((error) => {
          console.error("Error copying content: ", error);
        });

      setCopied(true);
      setTimeout(() => {
        setCopied(false);
      }, 3000);
    } else {
      console.error("divRef is null");
    }
  };

  const generateUniqueId = () => {
    return Date.now().toString();
  };

  // const addContent = () => {
  //   if (!content) return []; // Kiểm tra nếu messages là undefined thì trả về một mảng trống

  //   return content((content: string) => {
  //     return [content];
  //   });
  // };
  useEffect(() => {
    console.log("Content:", content);
  }, [content]); // Đảm bảo rằng useEffect sẽ chạy lại mỗi khi giá trị của content thay đổi

  const [sentQuestion, setSentQuestion] = useState(false);

  const handleSendQuestion = () => {
    // Xử lý việc gửi câu hỏi ở đây
    // Sau khi gửi xong, set state sentQuestion thành true
    setSentQuestion(true);
  };
  const [answeredContent, setAnsweredContent] = useState("");

  const fetcher = (url: string): Promise<any> => {
    return fetch(url).then((r) => r.json());
  };

  const urlApi = "/api/getMessage";

  const { data, error, isLoading } = useSWR(urlApi, fetcher);

  console.log(data);

  return (
    <div className="w-full dark:bg-gray-900 container">
      <div>
        {messages && messages.length === 0 ? (
          <div className="flex justify-center items-center mt-72 ">
            <img
              src="https://png.pngtree.com/png-clipart/20230207/original/pngtree-beauty-logo-design-png-image_8947095.png"
              alt="img"
              width={"100px"}
              height={"100px"}
            />
            <p className="lg:text-xl md:text-lg sm:text-sm text-black font-bold">
              Chat AI
            </p>
          </div>
        ) : (
          <div>
            {messages.map((message: string, index: number) => (
              <div key={content.parent}>
                <div className="p-5 mb-4">
                  <div>
                    <div className="flex space-x-1">
                      <img
                        src="https://png.pngtree.com/png-clipart/20230207/original/pngtree-beauty-logo-design-png-image_8947095.png"
                        alt="img"
                        width={"30px"}
                        height={"30px"}
                      />
                      <p className="font-bold text-black lg:text-base md:text-sm sm:text-xs  pt-1">
                        Name
                      </p>
                    </div>
                    <div>
                      <pre className="overflow-x-auto">
                        <p
                          className="whitespace-pre-wrap text-black"
                          key={content.parent}
                        >{`${message}`}</p>
                      </pre>
                    </div>
                  </div>

                  <div>
                    <div className="flex space-x-1 mt-10">
                      <img
                        src="https://png.pngtree.com/templates/sm/20180519/sm_5b001cfa899b9.jpg"
                        alt="img"
                        width={"30px"}
                        height={"30px"}
                      />
                      <p className="font-extrabold lg:text-base md:text-sm sm:text-xs pt-1 text-black">
                        Chat AI
                      </p>
                    </div>

                    {loading && <div>Loading...</div>}

                    {content && (
                      <div>
                        {!loading && content && (
                          <div className="text-base pt-2" key={content.parent}>
                            {!content.error && (
                              <div className="lg:text-base md:text-sm sm:text-xs mx-auto text-black">
                                <div>{content.parts}</div>

                                <div className="flex items-end justify-between mt-auto">
                                  <div className="flex items-center -space-x-10 mt-2">
                                    <button
                                      className=""
                                      onClick={() => handleCopyCode()}
                                    >
                                      <Save className="ml-0 hover:text-gray-500" />
                                    </button>

                                    <button
                                      className=""
                                      disabled={isChanging}
                                      onClick={() => handleChangeMessage()}
                                    >
                                      <Resert className="ml-0 hover:text-gray-500" />
                                    </button>

                                    <button
                                      className=""
                                      onClick={() =>
                                        handleDislikeClick(
                                          String(content.parent)
                                        )
                                      }
                                    >
                                      <Dislike className="ml-0 hover:text-gray-500" />
                                    </button>
                                  </div>
                                </div>
                                <div>
                                  {showQuestions &&
                                    !thankYou &&
                                    showAnswers[content.parent] && (
                                      <div className="border border-solid bg-white border-gray-300 rounded-lg mt-5 h-auto">
                                        <div className="flex justify-between ">
                                          <p className="m-3 lg:text-base md:text-sm sm:text-xs text-black dark:text-gray-100">
                                            Tell us more:
                                          </p>
                                          <span
                                            onClick={() =>
                                              setShowQuestions(false)
                                            }
                                          >
                                            <svg
                                              xmlns="http://www.w3.org/2000/svg"
                                              width="16"
                                              height="16"
                                              fill="currentColor"
                                              viewBox="0 0 16 16"
                                              className="m-3 cursor-pointer "
                                            >
                                              <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8z" />
                                            </svg>
                                          </span>
                                        </div>
                                        {showModal && (
                                          <div className="fixed inset-0 z-50 flex items-center justify-center overflow-x-hidden overflow-y-auto outline-none focus:outline-none">
                                            <div className="relative w-full sm:max-w-sm md:max-w-md lg:max-w-lg xl:max-w-xl mx-auto my-6">
                                              {/* Modal content */}
                                              <div className="relative flex flex-col w-full bg-white dark:bg-gray-800 border-2 border-gray-400 dark:border-gray-700 rounded-xl shadow-lg outline-none focus:outline-none">
                                                <div className="flex items-center justify-between p-5 border-b border-solid border-gray-400 dark:border-gray-700 rounded-t">
                                                  <h3 className="text-lg font-semibold text-black dark:text-gray-200 ">
                                                    Feedback Modal
                                                  </h3>
                                                  <button
                                                    className="p-1 ml-auto bg-transparent border-0 text-gray-700 float-right text-2xl leading-none font-semibold outline-none focus:outline-none"
                                                    onClick={cloneModal}
                                                  >
                                                    <span className="bg-transparent text-black dark:text-gray-200 h-7 w-7 text-3xl block outline-none focus:outline-none">
                                                      ×
                                                    </span>
                                                  </button>
                                                </div>
                                                {/* Body */}
                                                <div className="relative p-6 flex-auto">
                                                  {/* Modal content here */}
                                                  <input
                                                    type="text"
                                                    placeholder="This is the content of the feedback modal."
                                                    className="w-full rounded-xl bg-gray-200 text-black "
                                                  />
                                                </div>
                                                {/* Footer */}
                                                <div className="flex items-center justify-end p-6 border-t border-solid border-gray-300 dark:border-gray-700 rounded-b">
                                                  <button
                                                    className="text-gray-200 w-20 h-10 bg-blue-600 rounded-xl background-transparent font-bold uppercase text-sm outline-none focus:outline-none ease-linear transition-all duration-150"
                                                    type="button"
                                                    onClick={() =>
                                                      handleCloseModal()
                                                    }
                                                  >
                                                    <p className="text-white">
                                                      Send
                                                    </p>
                                                  </button>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        )}
                                        {feedback.map((feed, index) => (
                                          <button
                                            className="m-2 lg:text-base md:text-sm sm:text-xs border-2 border-solid border-gray-100 rounded-lg w-auto h-8  hover:bg-gray-300 dark:hover:bg-gray-700"
                                            key={index}
                                            onClick={() => {
                                              if (feed.id === 6) {
                                                openModal();
                                              } else {
                                                handleQuestionClick();
                                              }
                                            }}
                                          >
                                            <div className="pl-2 pr-2 lg:text-base md:text-sm sm:text-xs text-black dark:text-gray-200">
                                              {feed.title}
                                            </div>
                                          </button>
                                        ))}
                                      </div>
                                    )}

                                  {thankYou && (
                                    <div className="text-center lg:text-base md:text-sm sm:text-xs mt-3 text-black">
                                      Thank you for your feedback!
                                    </div>
                                  )}
                                </div>
                              </div>
                            )}
                            <h1>{content.error}</h1>
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
      <div ref={messagesEndRef} />
    </div>
  );
};

export default MessageList;
