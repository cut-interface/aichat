"use client";
import React, { FC, useState, useEffect } from "react";
import CartProduct from "@/components/A_cartProduct/Cart1";
import { PostAuthorType, PostProduct } from "@/data/types";
import Data from "@/data/jsons/__product.json";
import MessageList from "./MessageList";
const product = Data;

export const WidgetAuthors: FC = () => {
  const [isMobile, setIsMobile] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [expandedProductId, setExpandedProductId] = useState<number | null>(
    null
  );
  const stringNumber: string = "10";
  const numberValue: number = parseInt(stringNumber, 10);
  const authorsDemo: PostAuthorType[] = [];
  const [selectedProduct, setSelectedProduct] = useState<PostProduct | null>(
    null
  );

  const [navRightVisible, setNavRightVisible] = useState(false);

  const toggleNavRight = () => {
    setNavRightVisible(!navRightVisible);
  };

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768); // Adjust the threshold as needed
    };

    handleResize(); // Initial check
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  // Hàm để xử lý việc mở và đóng thông tin chi tiết của sản phẩm
  const toggleExpand = (productId: number) => {
    if (expandedProductId === productId) {
      // Nếu sản phẩm đã được mở, đóng nó lại
      setExpandedProductId(null);
    } else {
      // Nếu sản phẩm chưa được mở, mở nó
      setExpandedProductId(productId);
    }
  };

  return (
    <div
      className={`nc-WidgetAuthors rounded-sm h-[91vh] overflow-y-auto ${
        isMobile ? "absolute right-0" : "fixed right-0"
      } flex flex-col flex-1 bg-gray-800 dark:bg-neutral-800 w-56`}
    >
      <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 m-2">
        <div className="flex justify-between mt-4">
          <div>
            <span className="mt-[2px] text-base text-white dark:text-neutral-400 ml-2 font-bold">
              List Products
            </span>
          </div>

          <div>
            <span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-chevron-double-right text-white"
                viewBox="0 0 16 16"
                onClick={toggleNavRight}
              >
                <path
                  fill-rule="evenodd"
                  d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708"
                />
                <path
                  fill-rule="evenodd"
                  d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708"
                />
              </svg>
            </span>
          </div>
        </div>

        <div className="border-b border-black mt-4 text-white "></div>
      </div>

      <div className="">
        <div className="flex flex-col justify-center items-center">
          {product.map((product) => (
            <div key={product.id}>
              <div
                className="p-2 w-48 rounded-xl mt-3 items-center hover:bg-neutral-200 dark:hover:bg-neutral-700"
                onClick={() => toggleExpand(product.id)}
              >
                <div
                  className={`nc-CardAuthor flex items-center h-14 space-x-3`}
                >
                  <img
                    src={product.img}
                    alt={product.title}
                    width={"40px"}
                    height={"40px"}
                    className="rounded-xl"
                  />

                  <div>
                    <span
                      className={`block mt-[2px] text-sm text-white  dark:text-neutral-400`}
                    >
                      {product.title}
                    </span>
                  </div>
                </div>
              </div>

              {expandedProductId === product.id && (
                <div className="space-y-3 justify-center items-center ml-4 mt-2">
                  <div className="flex space-x-2"></div>

                  <div className="mt-2">
                    <span className="text-sm font-bold text-gray-400 ">
                      {product.download}
                    </span>
                  </div>

                  <div key={product.id}>
                    <button className="w-28 h-10 rounded-3xl bg-orange-400 hover:bg-orange-200">
                      <p className="text-xs">New chat</p>
                    </button>
                  </div>

                  <div className="mt-2">
                    <span className="text-sm font-bold text-gray-400 ">
                      {product.desc}
                    </span>
                  </div>
                </div>
              )}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default WidgetAuthors;
