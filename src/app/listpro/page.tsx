"use client";
import React, { useState, useEffect } from 'react';
import { DEMO_POSTS } from "@/data/posts";
import SectionLatestPosts from "@/components/A_ListPro/SectionLatestPosts";
import Navleft from "@/components/navleft/navleft";

const POSTS = DEMO_POSTS;
const MAGAZINE1_POSTS = POSTS.filter((_, i) => i >= 0 && i < 8);
const MAGAZINE2_POSTS = DEMO_POSTS.filter((_, i) => i >= 0 && i < 7);

const PageHomeDemo2: React.FC = () => {
  const [shouldHideNavleft, setShouldHideNavleft] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      // Set the condition to hide/show based on the screen width
      if (window.innerWidth <= 900) {
        setShouldHideNavleft(true);
      } else {
        setShouldHideNavleft(false);
      }
    };

    // Call handleResize when the window size changes
    window.addEventListener('resize', handleResize);

    // Call handleResize immediately when the component is mounted
    handleResize();

    // Cleanup event listener when the component is unmounted
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div className="nc-PageHomeDemo2 relative container">

      {!shouldHideNavleft && (
        <div className="fixed h-screen left-0">
          <Navleft />
        </div>
      )}


      <SectionLatestPosts
        posts={DEMO_POSTS.filter((_, i) => i >= 2 && i < 12)}
      />
      
    </div>
  );
};

export default PageHomeDemo2;