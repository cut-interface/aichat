"use client";

import React, { Fragment, useEffect, useMemo, useState, useRef } from "react";
import { usePathname } from "next/navigation";
import HeaderLogged from "@/components/header1/HeaderLogged";
import Header from "@/components/header1/Header";
import Header1 from "@/components/Header/Header";
import Icon from "@/components/Support/Icon";
import Header2 from "@/components/header1/Header1";
import {
  ShoppingBagIcon as ShoppingCartIcon,
  Cog8ToothIcon as CogIcon,
} from "@heroicons/react/24/outline";
import { Popover, Transition } from "@headlessui/react";
import SwitchDarkMode2 from "@/components/SwitchDarkMode/SwitchDarkMode2";
import { useThemeMode } from "@/hooks/useThemeMode";
import Navleft from "./Navleft1";
import Textarea from "@/components/Textarea/Textarea";
import data from "@emoji-mart/data";
import Picker from "@emoji-mart/react";

const SiteHeader = () => {
  let pathname = usePathname();
  useThemeMode();
  //

  //
  // FOR OUR DEMO PAGE, use do not use this, you can delete it.
  const [headerSelected, setHeaderSelected] = useState<
    "Header 1" | "Header 2" | "Header 3"
  >("Header 1");
  const [themeDir, setThemeDIr] = useState<"rtl" | "ltr">("ltr");
  const [isMobile, setIsMobile] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [messages, setMessages] = useState<Message[]>([]);
  const messagesScroll = useRef<HTMLDivElement>(null);
  const [showPicker, setShowPicker] = useState(false);

  const handleTogglePicker = () => {
    setShowPicker(!showPicker);
  };

  const handleToggleForm = () => {
    setIsOpen(!isOpen);
  };

  const handleEmojiSelect = (emoji: any) => {
    const input = document.querySelector('input[name="message"]') as HTMLInputElement; // Chỉ định kiểu của biến input là HTMLInputElement
    if (input) {
      input.value = emoji.native;
    }
};

  type Message = {
    question: string;
    answer: string;
  };
  const handleMessageSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const newMessage = e.currentTarget.message.value;
    const defaultMessage = "Câu trả lời ";

    if (newMessage.trim() !== "") {
      setMessages((prevMessages) => [
        ...prevMessages,
        { question: newMessage, answer: defaultMessage },
      ]);
    }

    e.currentTarget.reset();
  };

  useEffect(() => {
    // scroll to the bottom when messages update
    if (messagesScroll.current) {
      messagesScroll.current.scrollTop = messagesScroll.current.scrollHeight;
    }
  }, [messages]);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768);
    };

    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  useEffect(() => {
    if (themeDir === "rtl") {
      document.querySelector("html")?.setAttribute("dir", "rtl");
    } else {
      document.querySelector("html")?.removeAttribute("dir");
    }
    return () => {
      document.querySelector("html")?.removeAttribute("dir");
    };
  }, [themeDir]);

  //
  const handleDeleteChat = () => {
    // Load lại trang
    window.location.reload();
  };

  const renderControlSelections = () => {
    return (
      <div className="ControlSelections relative z-40 hidden md:block">
        <div className="fixed right-10 bottom-10 z-40 flex items-center">
          <Popover className="relative">
            {({ open, close }) => (
              <>
                <Popover.Button
                  className={`p-2.5 bg-gray-300 hover:bg-gray-400 dark:bg-primary-6000 dark:hover:bg-primary-700 rounded-full shadow-xl border border-gray-400 dark:border-primary-6000 z-10 focus:outline-none ${
                    open ? " focus:ring-2 ring-primary-500" : ""
                  }`}
                >
                  <span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="40"
                      height="40"
                      fill="currentColor"
                      className="bi bi-chat-dots-fill m-1 text-gray-800"
                      viewBox="0 0 16 16"
                    >
                      <path d="M16 8c0 3.866-3.582 7-8 7a9 9 0 0 1-2.347-.306c-.584.296-1.925.864-4.181 1.234-.2.032-.352-.176-.273-.362.354-.836.674-1.95.77-2.966C.744 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7M5 8a1 1 0 1 0-2 0 1 1 0 0 0 2 0m4 0a1 1 0 1 0-2 0 1 1 0 0 0 2 0m3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2" />
                    </svg>
                  </span>
                </Popover.Button>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-200"
                  enterFrom="opacity-0 translate-y-1"
                  enterTo="opacity-100 translate-y-0"
                  leave="transition ease-in duration-150"
                  leaveFrom="opacity-100 translate-y-0"
                  leaveTo="opacity-0 translate-y-1"
                >
                  <Popover.Panel className="absolute right-0 bottom-20 z-10 mt-3 w-screen max-w-md">
                    <div className="rounded-2xl bg-white dark:bg-neutral-950 overflow-hidden nc-custom-shadow-1">
                      <div className="relative p-6  bg-[#5356FF] flex justify-between">
                        <div className="flex items-center space-x-2">
                          <div className="">
                            <img
                              src="https://cdn.sforum.vn/sforum/wp-content/uploads/2024/01/avartar-anime-51.jpg"
                              alt=""
                              className="rounded-full"
                              width={"50px"}
                            />
                          </div>
                          <span className="text-base font-bold text-white">
                            Chat AI
                          </span>
                        </div>

                        <div className="flex justify-center items-center space-x-4">
                          <div
                            className="flex justify-center items-center rounded-full w-10 h-10 bg-gray-200"
                            onClick={() => setIsOpen(!isOpen)}
                          >
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="19"
                                height="19"
                                fill="currentColor"
                                className="bi bi-three-dots"
                                viewBox="0 0 16 16"
                              >
                                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3" />
                              </svg>
                            </span>
                          </div>

                          {isOpen && (
                            <div className="absolute bg-[#DFF5FF] rounded-lg shadow-md p-3 top-20 right-20 z-50">
                              <ul className="space-y-4 text-sm font-mono">
                                <li className="hover:font-bold flex space-x-2 items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-browser-chrome" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M16 8a8 8 0 0 1-7.022 7.94l1.902-7.098a3 3 0 0 0 .05-1.492A3 3 0 0 0 10.237 6h5.511A8 8 0 0 1 16 8M0 8a8 8 0 0 0 7.927 8l1.426-5.321a3 3 0 0 1-.723.255 3 3 0 0 1-1.743-.147 3 3 0 0 1-1.043-.7L.633 4.876A8 8 0 0 0 0 8m5.004-.167L1.108 3.936A8.003 8.003 0 0 1 15.418 5H8.066a3 3 0 0 0-1.252.243 2.99 2.99 0 0 0-1.81 2.59M8 10a2 2 0 1 0 0-4 2 2 0 0 0 0 4"/>
</svg>
                                  <a href="chatai">Đi đến web chat</a>
                                </li>
                                <li className="hover:font-bold flex space-x-2 items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-plus-lg" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2"/>
</svg>
                                  <a href="chatai">New Chat</a>
                                </li>
                                <li className="hover:font-bold flex space-x-2 items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash3" viewBox="0 0 16 16">
  <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5"/>
</svg>
                                  <a href="#" onClick={handleDeleteChat}>
                                    Delete Chat
                                  </a>
                                </li>
                              </ul>
                            </div>
                          )}
                          
                          <div className="flex justify-center items-center rounded-full w-10 h-10 bg-gray-200" onClick={close}>
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="19"
                                height="19"
                                fill="currentColor"
                                className="bi bi-chevron-down "
                                viewBox="0 0 16 16"
                                
                              >
                                <path
                                  fill-rule="evenodd"
                                  d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708"
                                />
                              </svg>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="border border-gray-400"></div>

                      <div
                        className="relative h-96 bg-[#67C6E3] overflow-y-auto"
                        ref={messagesScroll}
                      >
                        <div className="text-center pt-3">
                          <p className="text-sm text-gray-600">
                            Bắt đầu trò chuyện nhanh với Chat AI
                          </p>
                          <p className="text-sm text-gray-600">
                            Thông tin của bạn được ẩn và cuộc trò chuyện chỉ
                          </p>
                          <p className="text-sm text-gray-600">
                            lưu trên trình duyệt web.
                          </p>
                        </div>

                        <div className=" text-sm mt-8 ml-3">
                          <div className="flex items-center space-x-3 min-w-max h-auto">
                            <img
                              src="https://cdn.sforum.vn/sforum/wp-content/uploads/2024/01/avartar-anime-51.jpg"
                              alt=""
                              className="rounded-full"
                              width={"30px"}
                            />
                            <p className="p-3 bg-[#DFF5FF] rounded-lg">
                              Hỏi tôi đi
                            </p>
                          </div>
                        </div>

                        {messages.map((message, index) => (
                          <div
                            key={index}
                            className="rounded-md p-3 mb-2 space-y-20"
                          >
                            <div className="font-medium text-sm mb-8 ml-16 break-all float-right">
                              <p className="min-w-auto h-auto p-3 bg-[#378CE7] rounded-lg text-white">
                                {message.question}
                              </p>
                            </div>

                            <div className="text-sm">
                              <div className="flex items-center space-x-3 min-w-max h-auto">
                                <img
                                  src="https://cdn.sforum.vn/sforum/wp-content/uploads/2024/01/avartar-anime-51.jpg"
                                  alt=""
                                  className="rounded-full"
                                  width={"30px"}
                                />
                                <p className="p-3 bg-[#DFF5FF] rounded-lg">
                                  {message.answer}
                                </p>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>

                      <form onSubmit={handleMessageSubmit}>
                        {showPicker && (
                          <div className="absolute bottom-20 right-2">
                         <Picker
  onSelect={(emoji: any) => handleEmojiSelect(emoji)} // Chỉ định kiểu dữ liệu cho tham số emoji
  set="apple"
  theme="dark"
  title="Pick your emoji…"
  emoji="point_up"
  showPreview={false}
  showSkinTones={false}
  perLine={9}
/>
                          </div>
                        )}

                        <div className="flex ">
                        <input
  type="text"
  className="w-full rounded-xl rounded-t-none border-none h-20 text-xs focus:border-none"
  placeholder="Nhập câu hỏi, nhấn Enter để gửi..."
  name="message" // Đặt tên cho ô input là "message"
/>

                          <button>
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="20"
                                height="20"
                                fill="currentColor"
                                className="bi bi-emoji-sunglasses right-3 bottom-7 absolute "
                                viewBox="0 0 16 16"
                                onClick={handleTogglePicker}
                              >
                                <path d="M4.968 9.75a.5.5 0 1 0-.866.5A4.5 4.5 0 0 0 8 12.5a4.5 4.5 0 0 0 3.898-2.25.5.5 0 1 0-.866-.5A3.5 3.5 0 0 1 8 11.5a3.5 3.5 0 0 1-3.032-1.75M7 5.116V5a1 1 0 0 0-1-1H3.28a1 1 0 0 0-.97 1.243l.311 1.242A2 2 0 0 0 4.561 8H5a2 2 0 0 0 1.994-1.839A3 3 0 0 1 8 6c.393 0 .74.064 1.006.161A2 2 0 0 0 11 8h.438a2 2 0 0 0 1.94-1.515l.311-1.242A1 1 0 0 0 12.72 4H10a1 1 0 0 0-1 1v.116A4.2 4.2 0 0 0 8 5c-.35 0-.69.04-1 .116" />
                                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0m-1 0A7 7 0 1 0 1 8a7 7 0 0 0 14 0" />
                              </svg>
                            </span>
                          </button>
                        </div>
                      </form>
                    </div>
                  </Popover.Panel>
                </Transition>
              </>
            )}
          </Popover>
        </div>
      </div>
    );
  };
  //

  const supportControlSelections = () => {
    return (
      <div className="ControlSelections relative z-40 hidden md:block">
        <div className="fixed right-10 bottom-10 z-40 flex items-center">
          <Popover className="relative">
            {({ open }) => (
              <>
                <Popover.Button
                  className={`w-20 h-20 border-solid bg-white hover:bg-neutral-100 dark:bg-primary-6000 dark:hover:bg-primary-700 rounded-full shadow-xl border-2 border-gray-400 dark:border-primary-6000 z-10 focus:outline-none ${
                    open ? " focus:line-2 ring-primary-500" : ""
                  }`}
                >
                  <Icon />
                </Popover.Button>

                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-200"
                  enterFrom="opacity-0 translate-y-1"
                  enterTo="opacity-100 translate-y-0"
                  leave="transition ease-in duration-150"
                  leaveFrom="opacity-100 translate-y-0"
                  leaveTo="opacity-0 translate-y-1"
                >
                  <Popover.Panel className="absolute right-0 z-10 mt-3 w-screen max-w-sm">
                    <div className="rounded-2xl bg-white dark:bg-neutral-950 overflow-hidden nc-custom-shadow-1">
                      <div className="relative p-6 space-y-3.5 xl:space-y-5">
                        <span className="text-xl font-semibold">Customize</span>
                        <div className="w-full border-b border-neutral-200 dark:border-neutral-700"></div>
                        {renderRadioThemeDir()}
                        {renderRadioHeaders()}
                        <div className="flex space-x-2 xl:space-x-4 rtl:space-x-reverse">
                          <span className="text-sm font-medium">Dark mode</span>
                          <SwitchDarkMode2 />
                        </div>
                      </div>
                      <div className="bg-gray-50 dark:bg-white/5 p-5">
                        <a
                          className="flex items-center justify-center w-full px-4 py-2 !rounded-xl text-sm font-medium bg-primary-6000 text-white hover:bg-primary-700"
                          href={
                            "https://themeforest.net/item/ncmaz-blog-news-magazine-nextjs-template/44412092"
                          }
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <ShoppingCartIcon className="w-4 h-4" />
                          <span className="ms-2">Buy this template</span>
                        </a>
                      </div>
                    </div>
                  </Popover.Panel>
                </Transition>
              </>
            )}
          </Popover>
        </div>
      </div>
    );
  };

  const renderRadioThemeDir = () => {
    return (
      <div>
        <span className="text-sm font-medium">Theme dir</span>
        <div className="mt-1.5 flex items-center space-x-2 rtl:space-x-reverse">
          {(["rtl", "ltr"] as ("rtl" | "ltr")[]).map((dir) => {
            return (
              <div
                key={dir}
                className={`py-1.5 px-3.5 flex items-center rounded-full font-medium text-xs cursor-pointer select-none uppercase ${
                  themeDir === dir
                    ? "bg-black dark:bg-neutral-200 text-white dark:text-black shadow-black/10 shadow-lg"
                    : "border border-neutral-300 dark:border-neutral-700 hover:border-neutral-400 dark:hover:border-neutral-500"
                }`}
                onClick={() => setThemeDIr(dir)}
              >
                {dir}
              </div>
            );
          })}
        </div>
      </div>
    );
  };
  const renderRadioHeaders = () => {
    return (
      <div>
        <span className="text-sm font-medium">Header styles</span>
        <div className="mt-1.5 flex items-center space-x-2 rtl:space-x-reverse">
          {["Header 1", "Header 2", "Header 3"].map((header) => {
            return (
              <div
                key={header}
                className={`py-1.5 px-3.5 flex items-center rounded-full font-medium text-xs cursor-pointer select-none ${
                  headerSelected === header
                    ? "bg-black dark:bg-neutral-200 text-white dark:text-black shadow-black/10 shadow-lg"
                    : "border border-neutral-300 dark:border-neutral-700 hover:border-neutral-400 dark:hover:border-neutral-500"
                }`}
                onClick={() =>
                  setHeaderSelected(
                    header as "Header 1" | "Header 2" | "Header 3"
                  )
                }
              >
                {header}
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  //

  const headerComponent = useMemo(() => {
    let HeadComponent = HeaderLogged;
    let HeaderComponent1 = Header1;

    if (pathname === pathname) {
      HeadComponent = Header;
    }

    if (pathname === "/search") {
      HeadComponent = Header2;
    }

    return <HeadComponent />;
  }, [pathname, headerSelected]);

  return (
    <>
      {headerComponent}
      {renderControlSelections()}
    </>
  );
};

export default SiteHeader;
