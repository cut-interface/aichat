// components/ChatForm.tsx
"use client"
import React, { useState } from 'react';
import axios from 'axios';

const ChatForm: React.FC = () => {
  const [question, setQuestion] = useState<string>('');
  const [response, setResponse] = useState<string>('');

  const sendQuestion = async () => {
    try {
      const conversation_id = "dXNlcjE=-MjAyNC0wNC0xMSAxNzozOTozOS4yNjMwNDM=";
      const parent = "ZFhObGNqRT0tTWpBeU5DMHdOQzB4TVNBeE56b3pPVG96T1M0eU5qTXdORE09-=kTO3EzN14iM4kTMzgjMxcTM";
      const parts = "Viết về con chó";

      const apiResponse = await axios.post<string>(
        '../api/message',
        { conversation_id, parent, parts, question }
      );
      setResponse(apiResponse.data); // Truy cập vào dữ liệu từ phản hồi API
    } catch (error) {
      console.error('Lỗi khi gửi yêu cầu đến API:', error);
    }
  };

  return (
    <div>
      <textarea
        value={question}
        onChange={(e) => setQuestion(e.target.value)}
        rows={4}
        cols={50}
      />
      <button onClick={sendQuestion}>Gửi</button>
      <div>Phản hồi từ API: {response}</div>
    </div>
  );
};

export default ChatForm;
