  "use client";
  import React, { useEffect, useState } from "react";
  import { DEMO_POSTS_NEWS } from "@/data/posts";
  import LeftPro from "@/components/A_detailsPro/LeftPro";
  import RightPro from "@/components/A_detailsPro/RightPro";
  import Header from "@/components/A_detailsPro/Header";
  import { TaxonomyType } from "@/data/types";
  import { Route } from "@/routers/types";
  import Navleft from "@/components/navleft/navleft";

  const MAGAZINE1_POSTS = DEMO_POSTS_NEWS.filter((_, i) => i >= 8 && i < 16);
  const MAGAZINE2_POSTS = DEMO_POSTS_NEWS.filter((_, i) => i >= 0 && i < 7);

  const TRAVEL_SUBCATS: TaxonomyType[] = [
    {
      id: 1,
      name: "New York",
      href: "/archive/the-demo-archive-slug" as Route,
      thumbnail:
        "https://images.pexels.com/photos/2179602/pexels-photo-2179602.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
      count: 132,
      color: "pink",
      taxonomy: "category",
    },
    // ... (rest of TRAVEL_SUBCATS)
  ];

  const Homepage: React.FC = () => {
    const [isMobile, setIsMobile] = useState(false);
    const [shouldHideNavleft, setShouldHideNavleft] = useState(false);

    useEffect(() => {
      const handleResize = () => {
        // Thiết lập điều kiện ẩn hiện dựa trên kích thước màn hình
        setIsMobile(window.innerWidth < 768);

        if (window.innerWidth <= 900) {
          setShouldHideNavleft(true);
        } else {
          setShouldHideNavleft(false);
        }
      };
  
      // Gọi handleResize khi kích thước màn hình thay đổi
      window.addEventListener('resize', handleResize);
  
      // Gọi handleResize ngay khi component được mount
      handleResize();
  
      // Cleanup event listener khi component unmount
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    }, []);
    
    return (
      <div className="relative ">




        <Header className="" />
        <div className={`flex ${isMobile ? 'flex-col' : 'flex-row'} lg:flex-row container min-h-screen `}>

        {!shouldHideNavleft && (
        <div className="fixed h-screen left-0 top-32">
          <Navleft />
        </div>
      )}

          <div className={isMobile ? "w-full" : "lg:w-4/12"}>
            <div className={`mt-24 lg:mt-24 lg:ml-14 md:ml-5 ${isMobile ? 'lg:mt-0 -ml-4' : 'lg:fixed z-10'}`}>
              <LeftPro />
            </div>
          </div>

          <div className={isMobile ? "w-full mt-8" : "w-full lg:w-8/12 mt-24 lg:mt-24 md:ml-20"}>
            <div className="relative overflow-hidden loading=lazy">
              <RightPro />
            </div>
          </div>
        </div>
      </div>
    );
  };

  export default Homepage;