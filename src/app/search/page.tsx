"use client";

import React, { useState, useEffect } from "react";
import { DEMO_POSTS } from "@/data/posts";
import { PostDataType } from "@/data/types";
import { DEMO_AUTHORS } from "@/data/authors";
import { DEMO_CATEGORIES } from "@/data/taxonomies";
import Heading from "@/components/Heading/Heading";
import Heading1 from "@/components/Heading/Heading1";
import Input1 from "@/components/Input/Input1";
import SectionSubscribe2 from "@/components/SectionSubscribe2/SectionSubscribe2";
import NcImage from "@/components/NcImage/NcImage";
import NcLink from "@/components/NcLink/NcLink";
import Card11 from "@/components/A_Search/Card11";
import ButtonCircle from "@/components/Button/ButtonCircle";
import CardAuthorBox2 from "@/components/A_Search/CardAuthorBox2";
import { ArrowRightIcon } from "@heroicons/react/24/solid";
import Category from "@/components/A_Search/Category";
const posts: PostDataType[] = DEMO_POSTS.filter((_, i) => i < 6);
const posts1: PostDataType[] = DEMO_POSTS.filter((_, i) => i < 4);
const cats = DEMO_CATEGORIES.filter((_, i) => i < 15);
const tags = DEMO_CATEGORIES.filter((_, i) => i < 32);
const authors = DEMO_AUTHORS.filter((_, i) => i < 12);

const FILTERS = [
  { name: "Most Recent" },
  { name: "Curated by Admin" },
  { name: "Most Appreciated" },
  { name: "Most Discussed" },
  { name: "Most Viewed" },
];

const TABS = ["Articles", "Categories", "Tags", "Authors"];

const PageSearch = ({}) => {
  let s =
    "Unlock your creativity with stock images, stock videos, stock photos, and more";

  const [tabActive, setTabActive] = useState(TABS[0]);

  const handleClickTab = (item: string) => {
    if (item === tabActive) {
      return;
    }
    setTabActive(item);
  };

  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768);
    };

    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div className={`nc-PageSearch`}>
      {isMobile && (
        <div className="w-full px-2 xl:max-w-screen-2xl mx-auto">
          <div className="relative aspect-w-16 aspect-h-9 lg:aspect-h-5 overflow-hidden z-0">
            <NcImage
              alt="search"
              fill
              containerClassName="absolute inset-0"
              src="https://images.pexels.com/photos/2138922/pexels-photo-2138922.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
              className="object-cover w-full h-full"
              sizes="(max-width: 1280px) 100vw, 1536px"
            />
          </div>
          {/* CONTENT */}
          <div className="relative container -mt-52 lg:-mt-20 md:-mt-80">
            <div className=" dark:bg-neutral-900 dark:border dark:border-neutral-700 p-5 lg:p-16  flex items-center">
              <header className="w-full max-w-3xl mx-auto text-center flex flex-col items-center">
                <h2 className="text-sm mt-10 sm:text-2xl lg:text-4xl font-semibold text-black ">
                  {s}
                </h2>
                <form
                  className="relative w-full mt-8 sm:mt-11 text-left"
                  method="post"
                >
                  <label
                    htmlFor="search-input"
                    className="text-neutral-500 dark:text-neutral-300"
                  >
                    <span className="sr-only">Search all icons</span>
                    <Input1
                      id="search-input"
                      type="search"
                      placeholder="Type and press enter"
                      sizeClass="pl-14 py-5 pe-5 md:ps-16"
                     
                    />
                    <ButtonCircle
                      className="absolute end-2.5 top-1/2 transform -translate-y-1/2"
                      size=" w-11 h-11"
                      type="submit"
                    >
                      <ArrowRightIcon className="w-5 h-5 rtl:rotate-180" />
                    </ButtonCircle>
                    <span className="absolute start-5 top-1/2 transform -translate-y-1/2 text-2xl md:start-6">
                      <svg
                        width="24"
                        height="24"
                        fill="none"
                        viewBox="0 0 24 24"
                      >
                        <path
                          stroke="currentColor"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="1.5"
                          d="M19.25 19.25L15.5 15.5M4.75 11C4.75 7.54822 7.54822 4.75 11 4.75C14.4518 4.75 17.25 7.54822 17.25 11C17.25 14.4518 14.4518 17.25 11 17.25C7.54822 17.25 4.75 14.4518 4.75 11Z"
                        ></path>
                      </svg>
                    </span>
                  </label>
                </form>
                <div className="w-full text-sm text-start mt-8 text-neutral-500 dark:text-neutral-300">
                  <div className="inline-block space-x-1.5 sm:space-x-2.5 rtl:space-x-reverse">
                    <span className="">Related:</span>
                    <NcLink className="inline-block font-normal" href="/search">
                      Image
                    </NcLink>
                    <NcLink className="inline-block font-normal" href="/search">
                      Video
                    </NcLink>
                    <NcLink className="inline-block font-normal" href="/search">
                      3D
                    </NcLink>
                    <NcLink className="inline-block font-normal" href="/search">
                      Frontend
                    </NcLink>
                  </div>
                </div>
              </header>
            </div>
          </div>
        </div>
      )}

      {isMobile && (
        <div className="container py-16 lg:pb-28 lg:pt-20 sm:pt-10 md:mt-20 space-y-16 lg:space-y-28">
          <main>
            <Heading1 />

            {tabActive === "Articles" && (
              <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-2 xl:grid-cols-3 gap-5 md:gap-8 mt-8 lg:mt-10">
                {posts.map((post) => (
                  <Card11 key={post.id} post={post} />
                ))}
              </div>
            )}
            
            {/* LOOP ITEMS POSTS */}
            {tabActive === "Authors" && (
              <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-5 md:gap-8 mt-8 lg:mt-10">
                {authors.map((author) => (
                  <CardAuthorBox2 key={author.id} author={author} />
                ))}
              </div>
            )}
          </main>

          {/* MORE SECTIONS */}
          {/* === SECTION 5 === */}
          <div className="relative py-16">
            <SectionSubscribe2 />
          </div>
          <div className="relative">
            <Heading />
            {tabActive === "Articles" && (
              <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-5 md:gap-8 mt-8 lg:mt-10">
                {posts1.map((post) => (
                  <Card11 key={post.id} post={post} />
                ))}
              </div>
            )}
          </div>

          <div className="relative">
            <Category />
          </div>
        </div>
      )}

      {!isMobile && (
        <div className="w-full px-2 xl:max-w-screen-2xl mx-auto">
          <div className="relative aspect-w-16 aspect-h-9 lg:aspect-h-5 overflow-hidden z-0">
            <NcImage
              alt="search"
              fill
              containerClassName="absolute inset-0"
              src="https://images.pexels.com/photos/2138922/pexels-photo-2138922.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
              className="object-cover w-full h-full"
              sizes="(max-width: 1280px) 100vw, 1536px"
            />
          </div>
          {/* CONTENT */}
          <div className="relative container md:-mt-80 lg:-mt-96">
            <div className="dark:bg-neutral-900 dark:border dark:border-neutral-700 p-5 lg:p-16 flex items-center">
              <header className="w-full max-w-3xl mx-auto text-center flex flex-col items-center">
                <h2 className="text-xl sm:text-2xl md:text-2xl lg:text-3xl lg:pt-10  font-semibold text-white">
                  {s}
                </h2>
                <form
                  className="relative w-full mt-8 sm:mt-11 text-left"
                  method="post"
                >
                  <label
                    htmlFor="search-input"
                    className="text-neutral-500 dark:text-neutral-300"
                  >
                    <span className="sr-only">Search all icons</span>
                    <Input1
                      id="search-input"
                      type="search"
                      placeholder="Type and press enter"
                      sizeClass="pl-14 py-5 pe-5 md:ps-16"
                      defaultValue=""
                    />
                    <ButtonCircle
                      className="absolute end-2.5 top-1/2 transform -translate-y-1/2"
                      size=" w-11 h-11"
                      type="submit"
                    >
                      <ArrowRightIcon className="w-5 h-5 rtl:rotate-180" />
                    </ButtonCircle>
                    <span className="absolute start-5 top-1/2 transform -translate-y-1/2 text-2xl md:start-6">
                      <svg
                        width="24"
                        height="24"
                        fill="none"
                        viewBox="0 0 24 24"
                      >
                        <path
                          stroke="currentColor"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="1.5"
                          d="M19.25 19.25L15.5 15.5M4.75 11C4.75 7.54822 7.54822 4.75 11 4.75C14.4518 4.75 17.25 7.54822 17.25 11C17.25 14.4518 14.4518 17.25 11 17.25C7.54822 17.25 4.75 14.4518 4.75 11Z"
                        ></path>
                      </svg>
                    </span>
                  </label>
                </form>
                <div className="w-full text-sm text-start mt-4 text-neutral-500 dark:text-neutral-300">
                  <div className="inline-block space-x-1.5 sm:space-x-2.5 rtl:space-x-reverse">
                    <span className="">Related:</span>
                    <NcLink className="inline-block font-normal" href="/search">
                      Image
                    </NcLink>
                    <NcLink className="inline-block font-normal" href="/search">
                      Video
                    </NcLink>
                    <NcLink className="inline-block font-normal" href="/search">
                      3D
                    </NcLink>
                    <NcLink className="inline-block font-normal" href="/search">
                      Frontend
                    </NcLink>
                  </div>
                </div>
              </header>
            </div>
          </div>
        </div>
      )}
      {/* HEADER */}

      {/* ====================== END HEADER ====================== */}

      {!isMobile && (
        <div className="container py-16 lg:pb-28 lg:pt-20 sm:pt-40 space-y-16 lg:space-y-28">
          <main>
            <Heading />

            {tabActive === "Articles" && (
              <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-2 xl:grid-cols-3 gap-5 md:gap-8 mt-8 lg:mt-10">
                {posts.map((post) => (
                  <Card11 key={post.id} post={post} />
                ))}
              </div>
            )}
            {/* LOOP ITEMS POSTS */}
            {tabActive === "Authors" && (
              <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-5 md:gap-8 mt-8 lg:mt-10">
                {authors.map((author) => (
                  <CardAuthorBox2 key={author.id} author={author} />
                ))}
              </div>
            )}
          </main>

          {/* MORE SECTIONS */}
          {/* === SECTION 5 === */}
          <div className="relative py-16">
            <SectionSubscribe2 />
          </div>
          <div className="relative">
            <Heading />
            {tabActive === "Articles" && (
              <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-5 md:gap-8 mt-8 lg:mt-10">
                {posts1.map((post) => (
                  <Card11 key={post.id} post={post} />
                ))}
              </div>
            )}
          </div>

          <div className="relative">
            <Category />
          </div>
        </div>
      )}
    </div>
  );
};

export default PageSearch;
