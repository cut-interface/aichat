import { NextResponse } from "next/server";
export async function GET(req: Request) {
  const { searchParams } = new URL(req.url);
  const parts = searchParams.get("parts"); 
  const conversation_id = searchParams.get("conversation_id"); 
  const parent = searchParams.get("parent"); 
  const header = {
    Accept: "application/json",
    "Content-Type": "application/json",
  };

  const apiURL = `http://164.92.105.13:5003/backend-api/chat?conversation_id=dXNlcjE=-MjAyNC0wNS0xMyAwODozMjo1MC40MDAzMDE=&parent=ZFhObGNqRT0tTWpBeU5DMHdOUzB4TXlBd09Eb3pNam8xTUM0ME1EQXpNREU9-=UDOwcTM34CM4ETO4UTNxcTM&parts=${parts}`;
  console.log(apiURL);
  
  const res = await fetch(apiURL, {
    method: "GET",
    headers: header,
    next: {
      revalidate: 0,
    },
  });

  const result = await res.json();
  
  return NextResponse.json(result);
}
