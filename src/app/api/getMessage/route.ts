import { NextResponse } from "next/server";
export async function GET(req: Request) {
  const { searchParams } = new URL(req.url);
  const id = searchParams.get("conversation_id"); 
  const header = {
    Accept: "application/json",
    "Content-Type": "application/json",
  };

  const apiURL = `http://164.92.105.13:5003/backend-api/messages?conversation_id=dXNlcjE=-MjAyNC0wNS0xMyAwODozMjo1MC40MDAzMDE=`;


  console.log(apiURL);

  const res = await fetch(apiURL, {
    method: "GET",
    headers: header,
    next: {
      revalidate: 0,
    },
  });

  const result = await res.json();
  return NextResponse.json(result);
}
