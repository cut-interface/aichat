"use client";
import React, { useState } from "react";
import { useMediaQuery } from "react-responsive";
import NavLeft from "@/components/A_ChatAi/MenuLeft";
import NavRight from "@/components/A_ChatAi/MenuRight";
import ChatInput from "@/components/A_ChatAi/ChatInput";
import MessageList from "@/components/A_ChatAi/MessageList";
import Heading from "@/components/A_ChatAi/HeadingChatAi";
import { FaArrowRight, FaArrowLeft } from "react-icons/fa";

const ChatContainer = () => {
  const isMobile = useMediaQuery({ maxWidth: 640 });
  const isTablet = useMediaQuery({ minWidth: 641, maxWidth: 1021 });

  const [messages, setMessages] = useState<string[]>([]);
  const [isNavRightOpen, setIsNavRightOpen] = useState(false);

  const toggleNavRight = () => {
    setIsNavRightOpen(!isNavRightOpen);
  };

  const handleSendMessage = (newMessage: string) => {
    setMessages([...messages, newMessage]);
  };


  return (
    <div className="relative bg-cover bg-center bg-fixed overflow-hidden">
      <div className="relative dark:bg-gray-900 ">
        <div className="fixed lg:left-56 w-full h-14 z-30 bg-gray-100 dark:bg-gray-900 text-black">
          <Heading />
        </div>

        <div className="fixed z-50">
          {!isMobile && !isTablet && <NavLeft />}
        </div>

        <div className="flex justify-between w-full">
          <div className="flex flex-col items-center justify-center relative w-full h-full">
         
              <ChatInput onSendMessage={handleSendMessage} messages={messages}/>
           
          </div>

          {isMobile && (
            <div className="fixed top-0 right-20 z-50">
              <button
                className=" text-white p-2 rounded-full"
                onClick={toggleNavRight}
              >
                {isNavRightOpen ? <FaArrowLeft /> : <FaArrowRight />}
              </button>
            </div>
          )}

          {(isNavRightOpen || (!isMobile && !isTablet)) && (
            <div className="border-x-2 border-solid top-0 z-30">
              <NavRight />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ChatContainer;
