"use client";

import React, { Fragment, useEffect, useMemo, useState } from "react";
import { usePathname } from "next/navigation";
import Navleft from "@/components/navleft/navleft";
import { useThemeMode } from "@/hooks/useThemeMode";

const Navleft1 = () => {
  let pathname = usePathname();
  useThemeMode();

  const headerComponent = useMemo(() => {
    let HeadComponent = Navleft;

    return <HeadComponent />;
  }, [pathname]);

  return <>{headerComponent}</>;
};

export default Navleft1;
