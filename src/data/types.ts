import { Route } from "@/routers/types";
import { StaticImageData } from "next/image";

//  ######  CustomLink  ######## //
export interface CustomLink {
  label: string;
  href: Route;
  targetBlank?: boolean;
}

//  ##########  PostDataType ######## //
export interface TaxonomyType {
  id: string | number;
  name: string;
  href: Route;
  count?: number;
  thumbnail?: string | StaticImageData;
  desc?: string;
  color?: TwMainColor | string;
  taxonomy: "category" | "tag";
}

export interface PostAuthorType {
  id: string | number;
  firstName: string;
  lastName: string;
  displayName: string;
  avatar: string | StaticImageData;
  bgImage?: string | StaticImageData;
  email?: string;
  count: number;
  desc: string;
  jobName: string;
  href: Route;
}

export interface PostMessages {
  id: string | number;
  firstName: string;
  lastName: string;
  displayName: string;
  avatar: string | StaticImageData;
  bgImage?: string | StaticImageData;
  email?: string;
  count: number;
  desc: string;
  jobName: string;
  href: Route;
}

export interface listMessage{
  id: string | number;
  render1: string;
  code: string;
  render2: string;
}

export interface keyapi {
  isPopular: string | number;
  name: string;
  secret: string;
  tracking: string;
  created: string;
  used: string;
  premi: string;
}

export interface feedback {
  id: string | number;
  title: string;
}


export interface history {
  id: string | number;
  title: string;
  data: string;
}


export interface ChangeModel {
  id: string | number;
  name: string;
  title: string;
  price: string;
  function1: string;
  function2: string;
  function3: string;
  function4: string;
  function5: string;
}

export interface PostProduct {
  index: number;
  id: string;
  title: string;
  desc: string;
  date: string;
  href: string;
  img: string;
  download: string;
  Question1: string;
  Question2: string;
  Question3: string;
  Question4: string;
}

export interface PostDataType {
  id: string | number;
  author: PostAuthorType;
  date: string;
  href: Route;
  categories: TaxonomyType[];
  title: string;
  featuredImage: string | StaticImageData;
  desc?: string;
  like: {
    count: number;
    isLiked: boolean;
  };
  bookmark: {
    count: number;
    isBookmarked: boolean;
  };
  commentCount: number;
  viewdCount: number;
  readingTime: number;
  postType: "standard" | "video" | "gallery" | "audio";
  videoUrl?: string;
  audioUrl?: string | string[];
  galleryImgs?: string[];
}

export type TwMainColor =
  | "pink"
  | "green"
  | "yellow"
  | "red"
  | "indigo"
  | "blue"
  | "purple"
  | "gray";
